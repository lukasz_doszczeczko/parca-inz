<?php defined('SYSPATH') or die('No direct script access.');


class Controller_App extends Controller_Welcome
{
    public function action_index()
    {
        if (Auth::instance()->logged_in()) {
            $page = ORM::factory('EmailSchedules')->where('MemberID', '=', Auth::instance()->get_user()->id);
            $firms = $page->find_all();
            $status = $this->request->query('status');

            $color = 'red';
            $message = 'Wpis został usunięty poprawnie.';

            if ($status == "deleted") {

            } elseif ($status == "addok") {
                $message = 'Użytkownik został dodany pomyślnie';
                //$this->createpdf();
                $color = 'green';
            } else {
                $color = 'black';
                $message = '';
            }
            $this->template->content = View::factory('app/start')
                ->set('firms', $firms)
                ->set('color', $color)
                ->set('message', $message);
        } else {
            $LTId = $this->request->query('LTId');
            if ($LTId != "") {
                $emailSchedule = ORM::factory('EmailSchedules')
                    ->where('Link', '=', $LTId)
                    ->find();

                $dbModel = ORM::factory('Databases')->where("Name", "=", $emailSchedule->DataBase)->find();
                $this->createpdf($dbModel->FullName, $dbModel->CompanyName, 1);
            } else
                $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }
    }

    public function action_addClient()
    {
        if (Auth::instance()->logged_in()) {
            $Model2 = ORM::factory('EmailSchedules');
            $dbModel = ORM::factory("Databases")->where("UserId", "=", Auth::instance()->get_user()->id)->find_all();
            $dbm = array();
            for ($i = 0; $i < $dbModel->count(); $i++) {
                $dbm[$dbModel[$i]->Name] = $dbModel[$i]->Name;
            }
            $smsModel = ORM::factory('SmsSchedules');
            $post = Validation::factory($_POST);
            $post->rule('Email', 'not_empty')
                ->rule('FirmName', 'not_empty')
                ->rule('FirmName', 'regex', array(':value', "/^([a-zA-Z0-9,\\/.\"'\\s])+$/"))
                ->rule('Email', array('valid', 'email'))
                ->rule('PhoneNumber', array('valid', 'phone'))
                ->rule('PhoneNumber', 'not_empty');

            /*if ((((!isset($_POST['ZiK']) || $_POST['ZiK'] != "on")) && ((!isset($_POST['RZiS']) || ($_POST['RZiS'] != "on"))))) {
                $post->rule('ZiK', 'not_empty');
            }*/


            //$Model = new Model_firms(); //tworzenie obiektu Model_Director który przed chwilą utworzyliśmy
            if ($this->request->post()) { //sprawdzenie czy przyszły dane z formularza
                if ($post->check()) {
                    /*$toSend = 0; //toSend - jakie dokumenty ma wysłać
                    if ((isset($_POST['RZiS'])) && $_POST['RZiS'] == "on") {
                        if ((isset($_POST['ZiK'])) && $_POST['ZiK'] == "on") {
                            $toSend = 3; //oba
                        } else {
                            $toSend = 1; //Rachunek zysków i strat
                        }
                    } else {
                        if (((isset($_POST['ZiK'])) && $_POST['ZiK'] == "on")) {
                            $toSend = 2; //Zysk i Koszty
                        }
                    }*/
                    $Mo = (isset($_POST['week']) && $_POST['week'] == "Mo") ? TRUE : FALSE; //Sprawdzanie dnia tygodnia do wysyłki
                    $Tu = (isset($_POST['week']) && $_POST['week'] == "Tu") ? TRUE : FALSE;
                    $We = (isset($_POST['week']) && $_POST['week'] == "We") ? TRUE : FALSE;
                    $Th = (isset($_POST['week']) && $_POST['week'] == "Th") ? TRUE : FALSE;
                    $Fr = (isset($_POST['week']) && $_POST['week'] == "Fr") ? TRUE : FALSE;
                    $Sa = (isset($_POST['week']) && $_POST['week'] == "Sa") ? TRUE : FALSE;
                    $Su = (isset($_POST['week']) && $_POST['week'] == "Su") ? TRUE : FALSE;


                    $Model2->Name = $_POST['FirmName']; //Add data to Model
                    $Model2->Email = $_POST['Email'];
                    $Model2->DataBase = $_POST['DataBase'];
                    $Model2->PhoneNumber = $_POST['PhoneNumber'];
                    $Model2->MemberID = Auth::instance()->get_user()->id;
                    $Model2->AccountantEmail=Auth::instance()->get_user()->email;
                    $Model2->ScheduleID = 1;
                    $Model2->ScheduleName = $_POST['FirmName'] . "_" . $_POST['Email'];
                    $Model2->Mo = True;
                    $Model2->Tu = False;
                    $Model2->We = False;
                    $Model2->Th = False;
                    $Model2->Fr = False;
                    $Model2->Sa = False;
                    $Model2->Su = False;
                    $Model2->Status = 1;
                    $Model2->CreateDate = Date::formatted_time();
                    $Model2->CreateEmail = "ldoszczeczko@gmail.com";
                    $Model2->Link = $this->getRandString(20);

                    $Model2->save(); //Add Entry to DB
                    /*$smsModel->phoneNumber = $_POST['PhoneNumber'];
                    $smsModel->companyID = Auth::instance()->get_user()->companyId;
                    $smsModel->save();*/
                    if ((isset($_POST['send'])) && $_POST['send'] == "on") {
                        $dbModel = ORM::factory('Databases')->where("Name", "=", $Model2->DataBase)->find();
                        $company = ORM::factory('Companies', Auth::instance()->get_user()->companyId);
                        $mailContent = '<html>' .
                            ' <head></head>' .
                            ' <body>' .
                            'Dzień dobry, Biuro Rachunkowe ' . $dbModel->CompanyName . ' przesyła Państwu raport ZUS wrzesien. Szczegóły w załączniku <br>

                <br>' .
                            ' - - - - - - - - - -<br>' .
                            'Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                            'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $Model2->Link . '">e-raport.pl</a>' .
                            ' </body>' .
                            '</html>';
                        $this->sendMail($_POST['Email'], $dbModel->FullName, $mailContent);
                        $this->sendMail(Auth::instance()->get_user()->email, $dbModel->FullName, $mailContent);
                    }
                    //$this->sendsms($_POST['PhoneNumber']);
                    unset($_POST); //Clear POST

                    HTTP::redirect('app/index?status=addok', 302); //przekierowanie
                } else {
                    $errors = $post->errors('EmailSchedules');
                    $this->template->content = View::factory('app/addClient')->bind('errors', $errors)->bind('DataBase', $dbm);
                }
            } else {
                $errors = '';
                $this->template->content = View::factory('app/addClient')->bind('errors', $errors)->bind('DataBase', $dbm);
            }
        } else {
            $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }
    }

    /*  public function checkEmailSchedules()
      {
          $date = date('N');
          $day = "";
          switch ($date) {
              case '1':
                  $day = 'Mo';
                  break;
              case '2':
                  $day = 'Tu';
                  break;
              case '3':
                  $day = 'We';
                  break;
              case '4':
                  $day = 'Th';
                  break;
              case '5':
                  $day = 'Fr';
                  break;
              case '6':
                  $day = 'Sa';
                  break;
              case '7':
                  $day = 'Su';
                  break;
          }
          $emailSchedulesModel = ORM::factory('EmailSchedules')->where($day, "=", 1)->find_all();
          foreach ($emailSchedulesModel as $key => $emailSchedule) {
              $dbModel = ORM::factory('Databases')->where("Name", "=", $emailSchedule->DataBase)->find();
              $this->sendMail($emailSchedule->Email, $emailSchedule->Link, $dbModel->FullName);
          }
      }*/

    public function action_checkSchedules()
    {
        ini_set('max_execution_time', 500);

        /*  $date = date('d');
          switch($date){
              case '09':
                  $this->sendMail()
                  $this->action_sendsms(0,"FIRMA XXX - Sierpień - PIT-4 = 159zł | ZUS - 51 = 1770,96 | 52 =  526,99 | 53 = 97,91");
                  break;
          }*/
        $emailSchedulesModel = ORM::factory('EmailSchedules')->find_all();

        try {
            foreach ($emailSchedulesModel as $key => $emailSchedule) {
                $this->checkEmailSchedules($emailSchedule);
                gc_collect_cycles();
            }
        } catch (Exception $e) {
            $this->template->content = $e->getMessage();
            $this->sendMailWithError("Check Schedules   ".$e->getMessage());
        }
        $this->checkSmsSchedules();
    }

    public function checkSmsSchedules()
    {

        try {
            $functions = new Functions();
            $currentMonth = $functions->getCurrentMonth();
            $currentDayOfMonth = $functions->getCurrentDayOfMonth();
            require_once 'modules/smsapi/Autoload.php';

            $client = new \SMSApi\Client('e-raport.pl');
            $client->setPasswordHash(md5('jN2ycyf1ga$t'));


            $smsapi = new \SMSApi\Api\SmsFactory();
            $smsapi->setClient($client);

            $phoneNumbers = null;

            switch ($currentDayOfMonth) {
                /*case '10':
                    $this->sendSmsWithIncomesCostsAndTotal($currentMonth);
                    $this->sendSmsWithPit($currentMonth);
                    break;*/

                case '15':
                    $this->sendSmsWithZus($currentMonth);
                    break;

                case '21':
                    //$this->sendSmsWithCit($currentMonth);
                    $this->sendSmsWithIncomesCostsAndTotal($currentMonth);
                    break;

                case '25':
                    // $this->sendSmsWithCit($currentMonth);
                    $this->sendSmsWithVat($currentMonth);
                    break;
            }
        } catch (Exception $e) {
            $this->sendMailWithError("Check SMS Schedules   ".$e->getMessage());
        }
    }

    public function checkEmailSchedules($emailSchedule)
    {
        try {
            $incomes = '';
            $costs = '';
            $totals = '';
            $message = '';
            $pit = '';
            $cit = '';
            $zusHealth = '';
            $zusSocial = '';
            $zusFgsp = '';
            $vat = '';

            $functions = new Functions();
            $currentDayOfMonth = $functions->getCurrentDayOfMonth();
            ini_set('max_execution_time', 500);

            $user = ORM::factory('Users', $emailSchedule->MemberID);
            $company = ORM::factory('Companies', $user->companyId);
            $dbModel = ORM::factory('Databases')->where("Name", "=", $emailSchedule->DataBase)->find();
            $accountantEmail = $emailSchedule->AccountantEmail;

            switch ($currentDayOfMonth) {

                /*case '12':
                    $smsSchedules = DB::select()->from('smsschedules')->where('emailScheduleId', '=', $emailSchedule->id)->where('type', '=', 1)->execute();
                    foreach ($smsSchedules as $item) {
                        $incomes = $item['income'];
                        $costs = $item['cost'];
                        $totals = $item['total'];
                    }
                    $message = '<html>' .
                        ' <head></head>' .
                        ' <body>' . //ToDo:Zmienić busacco
                        'Dzień dobry, Biuro Rachunkowe Busacco przesyła Państwu raport ZUS wrzesien. Szczegóły w raporcie <br>
                    Przychód:      ' . $incomes . '<br>
                    Koszty:        ' . $costs . '<br>
                    Dochód:        ' . $totals . '<br>

                    Termin Zapłaty: 25.10.2014r<br>
                    Numer rachunku bankowego Urzędu Skarbowego: XXXXXXXXXXXXXXXXXX<br>
                    Tytuł przelewu: 15.11.2014-1
                    <br>' .
                        ' - - - - - - - - - -<br>' .
                        'Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                        'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>' .
                        ' </body>' .
                        '</html>';

                    $this->sendMail($emailSchedule->Email, $emailSchedule->Link, $dbModel->FullName, $dbModel->CompanyName, $message);
                    break;*/

                case '14':
                    try {
                        $companyName = "";
                        $smsSchedules = DB::select()->from('smsschedules')->where('emailScheduleId', '=', $emailSchedule->id)->where('type', '=', 4)->execute();
                        foreach ($smsSchedules as $item) {
                            $zusHealth = $item['zusHealth'];
                            $zusSocial = $item['zusSocial'];
                            $zusFgsp = $item['zusFgsp'];
                            $companyName = $item["companyName"];
                        }
                        $message = '<html>' .
                            ' <head></head>' .
                            ' <body>' . //ToDo:Zmienić busacco
                            '<div style="background:#fafafa;">

        <div style="padding:15px 25px; border:1px solid #ccc; margin:auto; margin-top:15px; margin-bottom:15px; max-width:740px;">
        <div style="text-align:center; margin-bottom:15px;"><a href="e-raport.pl/app/index"><img src="http://e-raport.pl/Content/logo.png" /></a></div>

        <p><b>Dzien dobry ' . $companyName . ',</b></p>
        <p>Biuro Rachunkowe Busacco przesyła Państwu raport ZUS Listopad. Szczegóły w raporcie <br>
                                       ZUS Ubezpieczenie Socjalne:         ' . $zusSocial . '<br>
                        ZUS Ubezpieczenie Zdrowotne:        ' . $zusHealth . '<br>
                        ZUS FP i FGSP:                      ' . $zusFgsp . '<br>

                        Termin Zapłaty: 15.01.2015r<br>
                        Numer rachunku bankowego Ubezpieczenie zdrowotne: 78 1010 1023 0000 2613 9520 0000<br>
        Ubezpieczenia spoeczne: 83 1010 1023 0000 2613 9510 00000<br>
        <hr />
        Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                            'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>
        <hr />
        </div>


        </div>' .

                            ' </body>' .
                            '</html>';

                        /* $message = '<html>' .
                                 ' <head></head>' .
                                 ' <body>' .
                                 'Dzień dobry, Biuro Rachunkowe Busacco przesyła Państwu raport ZUS wrzesien. Szczegóły w załączniku <br>
                             ZUS Ubezpieczenie Socjalne:         ' . $zusSocial . '<br>
                             ZUS Ubezpieczenie Zdrowotne:        ' . $zusHealth . '<br>
                             ZUS FP i FGSP:                      ' . $zusFgsp . '<br>
                             <br>' .
                                 ' - - - - - - - - - -<br>' .
                                 'Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                                 'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>' .
                                 ' </body>' .
                                 '</html>';*/
                        $this->sendMail($emailSchedule->Email, $dbModel->FullName, $message);
                        $this->sendMail($emailSchedule->AccountantEmail, $dbModel->FullName, $message);

                        break;
                    } catch (Exception $e) {
                    }

                case '20':
                    try {
                        $companyName = "";
                        $smsSchedules = DB::select()->from('smsschedules')->where('emailScheduleId', '=', $emailSchedule->id)->where('type', '=', 2)->or_where('type', '=', 1)->or_where('type', '=', 3)->execute();
                        foreach ($smsSchedules as $item) {
                            if ($item["type"] == 2)
                                $cit = $item['cit'];
                            if ($item["type"] == 1) {
                                $incomes = $item['income'];
                                $costs = $item['cost'];
                                $totals = $item['total'];
                            }
                            if ($item["type"] == 3) {
                                $pit = $item['pit-4'];
                            }
                            $companyName = $item["companyName"];
                        }
                        $message = '<html>' .
                            ' <head></head>' .
                            ' <body>' . //ToDo:Zmienić busacco
                            '<div style="background:#fafafa;">

    <div style="padding:15px 25px; border:1px solid #ccc; margin:auto; margin-top:15px; margin-bottom:15px; max-width:740px;">
    <div style="text-align:center; margin-bottom:15px;"><a href="e-raport.pl/app/index"><img src="http://e-raport.pl/Content/logo.png" /></a></div>

    <p><b>Dzien dobry ' . $companyName . ',</b></p>
    <p>Biuro Rachunkowe Busacco przesyła Państwu raport CIT, PIT-4 oraz Pzychody, Koszty i Dochody za grudzień. Szczegóły w raporcie <br>
                    CIT:         ' . $cit . '<br>
                    PIT-4:         ' . $pit . '<br>
                    Przychód:      ' . $incomes . '<br>
                    Koszty:        ' . $costs . '<br>
                    Dochód:        ' . $totals . '<br>

                    Termin Zapłaty: 20.01.2015r<br>
                    Numer rachunku bankowego Urzędu Skarbowego: 15101015990039422222000000<br>
                    Tytuł przelewu: 14M12</p>
    <hr />
    Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                            'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>
    <hr />
    </div>


    </div>' .

                            ' </body>' .
                            '</html>';
                        /* $message = '<html>' .
                             ' <head></head>' .
                             ' <body>' .
                             'Dzień dobry, Biuro Rachunkowe Busacco przesyła Państwu raport CIT, PIT-4 oraz Pzychody, Koszty i Dochody za listopad. Szczegóły w załączniku <br>
                         CIT:         ' . $cit . '<br>
                         PIT-4:         ' . $pit . '<br>
                         Przychód:      ' . $incomes . '<br>
                         Koszty:        ' . $costs . '<br>
                         Dochód:        ' . $totals . '<br>

                         Termin Zapłaty: 20.11.2014r<br>
                         Numer rachunku bankowego Urzędu Skarbowego: <br>
                         Tytuł przelewu: 15.11.2014-1
                         <br>' .
                             ' - - - - - - - - - -<br>' .
                             'Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                             'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>' .
                             ' </body>' .
                             '</html>';*/
                        $this->sendMail($emailSchedule->Email, $dbModel->FullName, $message);
                        $this->sendMail($emailSchedule->AccountantEmail, $dbModel->FullName, $message);

                        break;
                    } catch (Exception $ex) {

                    }
                case '25':
                    try {
                        $companyName = "";
                        $smsSchedules = DB::select()->from('smsschedules')->where('emailScheduleId', '=', $emailSchedule->id)->where('type', '=', 5)->execute();
                        foreach ($smsSchedules as $item) {
                            $vat = $item['vat'];
                            $companyName = $item["companyName"];
                        }
                        $message = '<html>' .
                            ' <head></head>' .
                            ' <body>' . //ToDo:Zmienić busacco
                            '<div style="background:#fafafa;">

        <div style="padding:15px 25px; border:1px solid #ccc; margin:auto; margin-top:15px; margin-bottom:15px; max-width:740px;">
        <div style="text-align:center; margin-bottom:15px;"><a href="e-raport.pl/app/index"><img src="http://e-raport.pl/Content/logo.png" /></a></div>

        <p><b>Dzien dobry ' . $companyName . ',</b></p>
        <p>Biuro Rachunkowe Busacco przesyła Państwu raport VAT wrzesien. Szczegóły w raporcie <br>
                        VAT:      ' . $vat . '<br>

                        Termin Zapłaty: 25.10.2014r<br>
                        Numer rachunku bankowego Urzędu Skarbowego: 15101015990039422222000000<br>
                        Tytuł przelewu: 14M10</p>
        <hr />
        Wiadomosc wygerowana automatycznie przez system e-raport<br>' .
                            'Raport możesz pobrać z systemu: <a href="e-raport.pl/app/index?LTId=' . $emailSchedule->Link . '">e-raport.pl</a>
        <hr />
        </div>


        </div>' .

                            ' </body>' .
                            '</html>';

                        $this->sendMail($emailSchedule->Email, $dbModel->FullName, $message);
                        $this->sendMail($emailSchedule->AccountantEmail, $dbModel->FullName, $message);

                        break;
                    } catch (Exception $e) {
                    }

            }

            unset($user);
            unset($company);
            unset($dbModel);
            $user = null;
            $company = null;
            $dbModel = null;
        } catch (Exception $e) {
            $this->sendMailWithError("checkEmailSchedules   ".$e->getMessage());
        }
    }

    public function getCompanyNameFromDb($db)
    {
        try {
            $return = "";
            $databases = DB::select()->from('databases')->where('FullName', '=', $db)->execute();
            foreach ($databases as $database) {
                $return = $database["CompanyName"];
            }
            return $return;
        } catch (Exception $e) {
            $this->sendMailWithError("getCompanyNameFromDb  ".$e->getMessage());
        }
    }

    public function sendMail($email, $dbName, $messageContent)
    {
        try {
            $report_data = ORM::factory('ReportData');
            $log = Log::instance();
            $log->add(Log::ALERT, "2.sendMail:        " . $dbName);
            //$fileName = $this->createpdf($dbName, $companyName);
            ini_set('max_execution_time', 500);
            require_once 'modules/swiftmailer-master/lib/swift_required.php';

            $transport = Swift_SmtpTransport::newInstance('serwer1495597.home.pl', 25)
                ->setUsername('automat@e-raport.pl')
                ->setPassword('#z5QEC!PvkDn');

            $mailer = Swift_Mailer::newInstance($transport);

            // Create the message
            $message = Swift_Message::newInstance()
                // Give the message a subject
                ->setSubject('e-raport.pl ' . Date::formatted_time())
                // Set the From address with an associative array
                ->setFrom(array('automat@e-raport.pl' => 'e-raport.pl'))
                // Set the To addresses with an associative array
                ->setTo(array($email))
                //->attach()//Swift_Attachment::fromPath($fileName)
                // Give it a body
                ->setBody($messageContent, 'text/html');


            $result = $mailer->send($message);
            //unlink($fileName);
        } catch (Exception $e) {
            $this->sendMailWithError("sendMail  ".$e->getMessage());
        }

    }

    public function sendMailWithError($messageContent)
    {
        $log = Log::instance();
        ini_set('max_execution_time', 500);
        require_once 'modules/swiftmailer-master/lib/swift_required.php';

        $transport = Swift_SmtpTransport::newInstance('serwer1495597.home.pl', 25)
            ->setUsername('automat@e-raport.pl')
            ->setPassword('#z5QEC!PvkDn');

        $mailer = Swift_Mailer::newInstance($transport);

        // Create the message
        $message = Swift_Message::newInstance()
            // Give the message a subject
            ->setSubject('ERROR e-raport.pl ' . Date::formatted_time())
            // Set the From address with an associative array
            ->setFrom(array('automat@e-raport.pl' => 'e-raport.pl'))
            // Set the To addresses with an associative array
            ->setTo(array('ldoszczeczko@gmail.com'))
            //->attach()//Swift_Attachment::fromPath($fileName)
            // Give it a body
            ->setBody($messageContent, 'text/html');


        $result = $mailer->send($message);
        //unlink($fileName);

    }

    public function createpdf($dbName, $companyName, $onlineFile = 0)
    {
        try {
            $log = Log::instance();
            $log->add(Log::ALERT, "3.createpdf:      " . $dbName);
            unset($report_data2);
            unset($report_data);
            unset($data);
            unset($data2);
            unset($address);
            $report_data = '';
            $report_data2 = '';
            if ($dbName == '15176927_klasteramfk10006') {
                $report_data = ORM::factory('ReportData2');
                $report_data2 = ORM::factory('ReportData2');
            } else {
                $report_data = ORM::factory('ReportData');
                $report_data2 = ORM::factory('ReportData');
            }


            $incomes = '';
            $costs = '';
            $total = '';
            $cits = '';
            $pits = '';
            $zusSoc = '';
            $zusHealth = '';
            $zusFgsp = '';
            $vatToPay = '';

            $incomes2 = '';
            $costs2 = '';
            $total2 = '';
            $cits2 = '';
            $pits2 = '';
            $zusSoc2 = '';
            $zusHealth2 = '';
            $zusFgsp2 = '';
            $vatToPay2 = '';

            $functions = new Functions();
            $currentYear = $functions->getCurrentYear();
            $previousYear = $currentYear - 1;

            $log->add(Log::INFO, "ROK TERAZNIEJSZY:     " . $currentYear);
            $log->add(Log::INFO, "ROK POPRZEDNI:        " . $previousYear);

            $report_data = $this->getbuf('buf_zapisy_2013', $dbName, $report_data,'buf_zapisy_2012');
            $data = $this->getbuf('zapisy_2013', $dbName, $report_data,'zapisy_2012');
            $data = $this->getVat($dbName, $data, $previousYear, 'zapisy_2013');

            $report_data2 = $this->getbuf('buf_zapisy_2014', $dbName, $report_data2,'buf_zapisy_2013');
            $data2 = $this->getbuf('zapisy_2014', $dbName, $report_data2,'zapisy_2013');
            $data2 = $this->getVat($dbName, $data2, $currentYear, 'buf_zapisy_2014');
            $address = "";

            $taxOfficeAccount = $this->get_taxOfficeAccount($dbName);

            $log = Log::instance();
            $log->add(Log::ALERT, "5a. adress:      " . $address);

            $address = $this->getConfig5($dbName);
            $log = Log::instance();
            $log->add(Log::ALERT, "5b.createpdf_address:      " . $address);

            $currentMonth = date('n');

            for ($i = 0; $i < 13; $i = $i + 1) {
                if ($i == 11) {
                    $incomes2 .= '<td style="color: red"><b>' . Num::format($data2->income[$i], 2, true) . '</b></td>';
                    $costs2 .= '<td style="color: red"><b>' . Num::format($data2->cost[$i], 2, true) . '</b></td>';
                    $total2 .= '<td style="color: red"><b>' . Num::format($data2->total[$i], 2, true) . '</b></td>';
                    $cits2 .= '<td style="color: red"><b>' . Num::format($data2->cit2pay[$i], 2, true) . '</b></td>';
                    $pits2 .= '<td style="color: red"><b>' . Num::format($data2->pit2get[$i], 2, true) . '</b></td>';
                    $zusSoc2 .= '<td style="color: red"><b>' . Num::format($data2->zusSocial[$i], 2, true) . '</b></td>';
                    $zusHealth2 .= '<td style="color: red"><b>' . Num::format($data2->zusHealth[$i], 2, true) . '</b></td>';
                    $zusFgsp2 .= '<td style="color: red"><b>' . Num::format($data2->zusFpFgsp[$i], 2, true) . '</b></td>';
                    $vatToPay2 .= '<td style="color: red"><b>' . Num::format($data2->vatToPay[$i], 2, true) . '</b></td>';
                } else {
                    $incomes2 .= '<td>' . Num::format($data2->income[$i], 2, true) . '</td>';
                    $costs2 .= '<td>' . Num::format($data2->cost[$i], 2, true) . '</td>';
                    $total2 .= '<td>' . Num::format($data2->total[$i], 2, true) . '</td>';
                    $cits2 .= '<td>' . Num::format($data2->cit2pay[$i], 2, true) . '</td>';
                    $pits2 .= '<td>' . Num::format($data2->pit2get[$i], 2, true) . '</td>';
                    $zusSoc2 .= '<td>' . Num::format($data2->zusSocial[$i], 2, true) . '</td>';
                    $zusHealth2 .= '<td>' . Num::format($data2->zusHealth[$i], 2, true) . '</td>';
                    $zusFgsp2 .= '<td>' . Num::format($data2->zusFpFgsp[$i], 2, true) . '</td>';
                    $vatToPay2 .= '<td>' . Num::format($data2->vatToPay[$i], 2, true) . '</td>';
                }
                $incomes .= '<td>' . Num::format($data->income[$i], 2, true) . '</td>';
                $costs .= '<td>' . Num::format($data->cost[$i], 2, true) . '</td>';
                $total .= '<td>' . Num::format($data->total[$i], 2, true) . '</td>';
                $cits .= '<td>' . Num::format($data->cit2pay[$i], 2, true) . '</td>';
                $pits .= '<td>' . Num::format($data->pit2get[$i], 2, true) . '</td>';
                $zusSoc .= '<td>' . Num::format($data->zusSocial[$i], 2, true) . '</td>';
                $zusHealth .= '<td>' . Num::format($data->zusHealth[$i], 2, true) . '</td>';
                $zusFgsp .= '<td>' . Num::format($data->zusFpFgsp[$i], 2, true) . '</td>';
                $vatToPay .= '<td>' . Num::format($data->vatToPay[$i], 2, true) . '</td>';
            }

            require_once 'modules/dompdf/dompdf_config.inc.php';
            $tmp = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            $tmp2 = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            for ($i = 0; $i < 13; $i = $i + 1) {
                $tmp[$i] = ($data->cit2get[$i] < $data->cit2pay[$i]) ? ($data->cit2pay[$i] - $data->cit2get[$i]) : 0;
                $tmp2[$i] = ($data->cit2get[$i] > $data->cit2pay[$i]) ? ($data->cit2get[$i] - $data->cit2pay[$i]) : 0;
            }
            for ($i = 0; $i < 13; $i = $i + 1) {
                $tmp3[$i] = ($data2->cit2get[$i] < $data2->cit2pay[$i]) ? ($data2->cit2pay[$i] - $data2->cit2get[$i]) : 0;
                $tmp4[$i] = ($data2->cit2get[$i] > $data2->cit2pay[$i]) ? ($data2->cit2get[$i] - $data2->cit2pay[$i]) : 0;
            }
            $html = '<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>

    <style type="text/css">
        table {
            border-collapse: collapse;
            font-size: 10px;
            text-align: right;
        }

        table td, table th {
            border: 1px solid black;
            width: 45px;
            height: 30px;
            border-top: 0;

            text-align: right;
            padding-right: 5px;
            font-weight: normal;
        }


      /*  table tr:first-child th {
            border-bottom: 1px;
        }*/

    /*
        table tr:first-child td{
            border-bottom: 1px;
        }
    */


        table tr:last-child td {
            border-bottom: 0;
            border-top: 1px;
        }

        table tr td:first-child,
        table tr th:first-child {
            border-left: 0;
            text-align: left;
        }

        table tr:nth-child(even){
            background-color: lightblue;
        }

        table tr td:last-child,
        table tr th:last-child {
            border-right: 0;
        }
    </style>
    <body>
    <table style="border: none">
        <tr style="border: none">
            <td style="font-size: 12px; width: 475px; border: none">
                ' . $address . '
            </td>
            <td style="border: none">
                <a href="http://www.e-raport.pl/app/"><img src="../Content/logo.png"
                                                      alt="Strona główna"/></a>
            </td>
        </tr>
    </table>
    <h1>Zysk/Strata/Podatki</h1>
    <br>
    <table style="border-top: 0; ">
        <tr style="border-bottom: 1px">
            <th >2014</th>
            <th>Styczen</th>
            <th>Luty</th>
            <th>Marzec</th>
            <th>Kwiecien</th>
            <th>Maj</th>
            <th>Czerwiec</th>
            <th>Lipiec</th>
            <th>Sierpien</th>
            <th>Wrzesien</th>
            <th>Pażdziernik</th>
            <th>Listopad</th>
            <th>Grudzien</th>
            <th>Suma</th>
        </tr>
        <tr>
            <td>Przychód</td>
            ' . $incomes . '
        </tr>
        <tr>
            <td>Koszty</td>
            ' . $costs . '
        </tr>
        <tr>
            <td>Dochód</td>
           ' . $total . '
        </tr>
        <tr>
            <td>CIT</td>
            ' . $cits . '
        </tr>
        <tr>
            <td>PIT-4</td>
            ' . $pits . '
        </tr>
        <tr>
            <td>ZUS <br>ub. socjalne</td>
            ' . $zusSoc . '
        </tr>
        <tr>
            <td>ZUS<br>ub. zdrowotne</td>
            ' . $zusHealth . '
        </tr>
        <tr>
            <td>ZUS FP<br>i PGSF</td>
            ' . $zusFgsp . '
        </tr>
        <tr>
            <td>VAT do zapł.</td>
            ' . $vatToPay . '
        </tr>

    </table>

    <br><table style="border-top: 0; ">
        <tr style="border-bottom: 1px">
            <th >2015</th>
            <th>Styczen</th>
            <th>Luty</th>
            <th>Marzec</th>
            <th>Kwiecien</th>
            <th>Maj</th>
            <th>Czerwiec</th>
            <th>Lipiec</th>
            <th>Sierpien</th>
            <th>Wrzesien</th>
            <th>Pażdziernik</th>
            <th>Listopad</th>
            <th>Grudzien</th>
            <th>Suma</th>
        </tr>
        <tr>
            <td>Przychód</td>
            ' . $incomes2 . '
        </tr>
        <tr>
            <td>Koszty</td>
            ' . $costs2 . '
        </tr>
        <tr>
            <td>Dochód</td>
           ' . $total2 . '
        </tr>
        <tr>
            <td>CIT</td>
            ' . $cits2 . '
        </tr>
        <tr>
            <td>PIT-4</td>
            ' . $pits2 . '
        </tr>
        <tr>
            <td>ZUS <br>ub. socjalne</td>
            ' . $zusSoc2 . '
        </tr>
        <tr>
            <td>ZUS<br>ub. zdrowotne</td>
            ' . $zusHealth2 . '
        </tr>
        <tr>
            <td>ZUS FP<br>i PGSF</td>
            ' . $zusFgsp2 . '
        </tr>
            <tr>
            <td>VAT do zapł.</td>
            ' . $vatToPay2 . '
        </tr>


        <tr style="font-size: 9px; padding:15px;">
    <br><br>
            Ubezpieczenia społeczne: 83 1010 1023 0000 2613 9510 0000<br><br>

            Ubezpieczenie zdrowotne: 78 1010 1023 0000 2613 9520 0000<br><br>

            Fundusz Pracy: 73 1010 1023 0000 2613 9530 0000 <br><br>

            Urząd Skarbowy: ' . $taxOfficeAccount . '<br>
            <br>

            <b>Legenda:</b>
        </tr>
    </table>
    </body>
    </html>';

            $dompdf = new DOMPDF();
            $fileName = $companyName . '_' . date('Y') . date('m') . date('d') . '_1.pdf';
            $dompdf->load_html($html, 'UTF-8');
            $dompdf->render();
            if ($onlineFile == 1) {
                $dompdf->stream($fileName);
            } else {
                $output = $dompdf->output();
                file_put_contents($fileName, $output);
            }
            return $fileName;
        } catch (Exception $e) {
            $this->sendMailWithError("createpdf     ".$e->getMessage());
        }
    }

    public function getbuf($tableName = 'Buf', $dbName, $data, $previousTableName)
    {
        try {//ToDo: jeśli miesiąc ==1 to zmienić 12
            $log = Log::instance();
            $log->add(Log::ALERT, "4.getbuf:      " . $dbName);
            unset($buf);
            $buf = null;
            $i = '';
            $functions = new Functions();
            /*$currentMonth = $functions->getCurrentIntMonth();*/
            $currentMonth = 13;
            $log->add(Log::INFO, "Teraźniejszy miesiąc:      " . $currentMonth);
            /////////////INCOMES/////////////
            foreach ($data->incomeTab as $incomeTabElement) {
                //$buf = ORM::factory($tableName)->where('synt', '=', $incomeTabElement)->find_all();

                $bufDec = DB::select()->from($tableName)->where('synt', '=', $incomeTabElement)->where('strona', '=', 1)->execute($dbName);

                $buf = DB::select()->from($tableName)->where('synt', '=', $incomeTabElement)->execute($dbName);

                /* $name = "Model_" . $tableName;
                 $buf = new $name($dbName);
                 $buf = $buf->where('synt', '=', $incomeTabElement)->find_all();*/


                foreach ($bufDec as $bufElementDec) {
                    $dateDec = $bufElementDec['dataokr'];
                    $dateDec = explode('-', $dateDec);
                    switch ($dateDec[1]) {
                        case '12':
                            $data->income[11] = $data->income[11] + $bufElementDec['kwota'];
                            $data->income[12] = $data->income[12] + $bufElementDec['kwota'];
                            break;
                    }
                }

                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    if ($date[1] < $currentMonth) {
                        switch ($date[1]) {
                            case '1':
                                $data->income[0] = $data->income[0] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '2':
                                $data->income[1] = $data->income[1] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '3':
                                $data->income[2] = $data->income[2] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '4':
                                $data->income[3] = $data->income[3] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '5':
                                $data->income[4] = $data->income[4] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '6':
                                $data->income[5] = $data->income[5] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '7':
                                $data->income[6] = $data->income[6] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '8':
                                $data->income[7] = $data->income[7] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '9':
                                $data->income[8] = $data->income[8] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '10':
                                $data->income[9] = $data->income[9] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            case '11':
                                $data->income[10] = $data->income[10] + $bufElement['kwota'];
                                $data->income[12] = $data->income[12] + $bufElement['kwota'];
                                break;
                            /*case '12':
                                $data->income[11] = $data->income[11] + $bufElement['kwota'];
                                break;*/
                        }
                    }
                    //$data->income[12] = $data->income[12] + $bufElement['kwota'];
                    /*if ($dbName="15176927_klasteramfk") {
                        $data->income[12]=56021.40;
                    }*/
                }
                $log->add(Log::INFO, 'Tabela:        ' . $tableName . '      Konto:     ' . $incomeTabElement . '      Kwota:      ' . $data->income[11]);
            }

            /////////////COSTS/////////////
            foreach ($data->costTab as $costTabElement) {
                    $bufDec = DB::select()->from($tableName)->where('synt', '=', $costTabElement)->where("strona", "=", 0)->execute($dbName);
                    foreach ($bufDec as $bufElementDec) {
                        $date = $bufElementDec['dataokr'];
                        $date = explode('-', $date);
                        if ($date[1] < $currentMonth) {
                            switch ($date[1]) {
                                case '12':
                                    $data->cost[11] = $data->cost[11] + $bufElementDec['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElementDec['kwota'];
                                    $log->add(LOG_INFO, "Koszty    , synt:    ".$costTabElement."    Kwota:    ".$bufElementDec['kwota']."    Data:".print_r($date));
                                    break;
                            }
                        }
                    }

                    $buf = DB::select()->from($tableName)->where('synt', '=', $costTabElement)->execute($dbName);
                    foreach ($buf as $bufElement) {
                        $date = $bufElement['dataokr'];
                        $date = explode('-', $date);
                        if ($date[1] < $currentMonth) {
                            switch ($date[1]) {
                                case '1':
                                    $data->cost[0] = $data->cost[0] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '2':
                                    $data->cost[1] = $data->cost[1] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '3':
                                    $data->cost[2] = $data->cost[2] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '4':
                                    $data->cost[3] = $data->cost[3] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '5':
                                    $data->cost[4] = $data->cost[4] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '6':
                                    $data->cost[5] = $data->cost[5] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '7':
                                    $data->cost[6] = $data->cost[6] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '8':
                                    $data->cost[7] = $data->cost[7] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '9':
                                    $data->cost[8] = $data->cost[8] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '10':
                                    $data->cost[9] = $data->cost[9] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                case '11':
                                    $data->cost[10] = $data->cost[10] + $bufElement['kwota'];
                                    $data->cost[12] = $data->cost[12] + $bufElement['kwota'];
                                    break;
                                /*                        case '12':
                                                            $data->cost[11] = $data->cost[11] + $bufElement['kwota'];
                                                            break;*/
                            }
                        }
                    }
            }

            /////////////TOTAL/////////////
            for ($i = 0; $i < 13; $i = $i + 1) {
                $data->total[$i] = $data->income[$i] - $data->cost[$i];
            }

            /* {
                     $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 1)->where('strona', '=', 0)->execute($dbName);
                     foreach ($buf as $bufElement) {
                         $date = $bufElement['dataokr'];
                         $date = explode('-', $date);
                         switch ($date[1]) {
                             case '1':
                                 $data->cit2get[0] = $data->cit2get[0] + $bufElement['kwota'];
                                 break;
                             case '2':
                                 $data->cit2get[1] = $data->cit2get[1] + $bufElement['kwota'];
                                 break;
                             case '3':
                                 $data->cit2get[2] = $data->cit2get[2] + $bufElement['kwota'];
                                 break;
                             case '4':
                                 $data->cit2get[3] = $data->cit2get[3] + $bufElement['kwota'];
                                 break;
                             case '5':
                                 $data->cit2get[4] = $data->cit2get[4] + $bufElement['kwota'];
                                 break;
                             case '6':
                                 $data->cit2get[5] = $data->cit2get[5] + $bufElement['kwota'];
                                 break;
                             case '7':
                                 $data->cit2get[6] = $data->cit2get[6] + $bufElement['kwota'];
                                 break;
                             case '8':
                                 $data->cit2get[7] = $data->cit2get[7] + $bufElement['kwota'];
                                 break;
                             case '9':
                                 $data->cit2get[8] = $data->cit2get[8] + $bufElement['kwota'];
                                 break;
                             case '10':
                                 $data->cit2get[9] = $data->cit2get[9] + $bufElement['kwota'];
                                 break;
                             case '11':
                                 $data->cit2get[10] = $data->cit2get[10] + $bufElement['kwota'];
                                 break;
                             case '12':
                                 $data->cit2get[11] = $data->cit2get[11] + $bufElement['kwota'];
                                 break;
                         }
                         $data->cit2get[12] = $data->cit2get[12] + $bufElement['kwota'];
                     }
                     for($i=0; $i<13; $i=$i+1)
                         $log->add(Log::INFO,$i.'2Tabela:        '.$tableName.'      Konto:     220       Kwota:      '.$data->cit2get[$i]);
                 }*/

            /////////////CIT/////////////
            {
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 1)->where('strona', '=', 1)->execute($dbName);
                $i = 0;
                foreach ($buf as $bufElement) {
                    $citDescription = explode(" ", trim($bufElement['opis']));
                    $isCit = $citDescription[0];
                    $citDescriptionDate = $citDescription[1];
                    /*
                                    $log->add(LOG_INFO,"CIT Description:  ".$citDescription[0]."+   ".$citDescription[1]);*/
                    if ($isCit == "CIT") {
                        $citDate = explode("/", $citDescriptionDate);
                        $month = $citDate[0];
                        $log->add(LOG_INFO, "CIT Description:  " . $citDescription[0] . "+   " . $citDescription[1] . "+   value:  " . $bufElement['kwota']);
                        switch ($month) {
                            case "01":
                                $data->cit2pay[0] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "02":
                                $data->cit2pay[1] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "03":
                                $data->cit2pay[2] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "04":
                                $data->cit2pay[3] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "05":
                                $data->cit2pay[4] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "06":
                                $data->cit2pay[5] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "07":
                                $data->cit2pay[6] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "08":
                                $data->cit2pay[7] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "09":
                                $data->cit2pay[8] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "10":
                                $data->cit2pay[9] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "11":
                                $data->cit2pay[10] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                            case "12":
                                $data->cit2pay[11] = $bufElement['kwota'];
                                $data->cit2pay[12] = $data->cit2pay[12] + $bufElement['kwota'];
                                break;
                        }

                    }
                }
                for ($i = 0; $i < 13; $i = $i + 1)
                    $log->add(Log::INFO, $i . 'Tabela:        ' . $tableName . '      Konto:     220       Kwota:      ' . $data->cit2pay[$i]);
            }

            /////////////PIT/////////////
            {
                $prevBuf = DB::select()->from($previousTableName)->where('synt', '=', 220)->where('poz1', '=', 2)->where('strona', '=', 1)->execute($dbName);
                foreach ($prevBuf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '12':
                            $data->pit2get[0] = $data->pit2get[0] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                    }
                }

                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 2)->where('strona', '=', 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '1':
                            $data->pit2get[1] = $data->pit2get[1] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '2':
                            $data->pit2get[2] = $data->pit2get[2] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '3':
                            $data->pit2get[3] = $data->pit2get[3] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '4':
                            $data->pit2get[4] = $data->pit2get[4] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '5':
                            $data->pit2get[5] = $data->pit2get[5] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '6':
                            $data->pit2get[6] = $data->pit2get[6] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '7':
                            $data->pit2get[7] = $data->pit2get[7] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '8':
                            $data->pit2get[8] = $data->pit2get[8] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '9':
                            $data->pit2get[9] = $data->pit2get[9] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '10':
                            $data->pit2get[10] = $data->pit2get[10] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                        case '11':
                            $data->pit2get[11] = $data->pit2get[11] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;
                       /* case '12':
                            $data->pit2get[11] = $data->pit2get[11] + $bufElement['kwota'];
                            $data->pit2get[12] = $data->pit2get[12] + $bufElement['kwota'];
                            break;*/
                    }

                }
            }

            /////////////ZUS_SOCIAL/////////////
            {
                $prevBuf = DB::select()->from($previousTableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 1)->where("strona", "=", 1)->execute($dbName);
                foreach ($prevBuf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '12':
                            $data->zusSocial[0] = $data->zusSocial[0] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                    }
                }
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 1)->where("strona", "=", 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '1':
                            $data->zusSocial[1] = $data->zusSocial[1] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '2':
                            $data->zusSocial[2] = $data->zusSocial[2] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '3':
                            $data->zusSocial[3] = $data->zusSocial[3] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '4':
                            $data->zusSocial[4] = $data->zusSocial[4] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '5':
                            $data->zusSocial[5] = $data->zusSocial[5] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '6':
                            $data->zusSocial[6] = $data->zusSocial[6] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '7':
                            $data->zusSocial[7] = $data->zusSocial[7] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '8':
                            $data->zusSocial[8] = $data->zusSocial[8] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '9':
                            $data->zusSocial[9] = $data->zusSocial[9] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '10':
                            $data->zusSocial[10] = $data->zusSocial[10] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        case '11':
                            $data->zusSocial[11] = $data->zusSocial[11] + $bufElement['kwota'];
                            $data->zusSocial[12] = $data->zusSocial[12] + $bufElement['kwota'];
                            break;
                        /*case '12':
                            $data->zusSocial[12] = $data->zusSocial[11] + $bufElement['kwota'];

                            break;*/
                    }

                }
                for ($i = 0; $i < 13; $i = $i + 1) {
                    $log->add(LOG_ALERT, "ZUS_SOCIAL: " . $data->zusSocial[$i]);
                }
            }

            /////////////ZUS_HEALTH/////////////
            {
                $prevBuf = DB::select()->from($previousTableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 2)->where("strona", "=", 1)->execute($dbName);
                foreach ($prevBuf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '12':
                            $data->zusHealth[0] = $data->zusHealth[0] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                    }
                }

                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 2)->where("strona", "=", 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '1':
                            $data->zusHealth[1] = $data->zusHealth[1] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '2':
                            $data->zusHealth[2] = $data->zusHealth[2] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '3':
                            $data->zusHealth[3] = $data->zusHealth[3] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '4':
                            $data->zusHealth[4] = $data->zusHealth[4] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '5':
                            $data->zusHealth[5] = $data->zusHealth[5] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '6':
                            $data->zusHealth[6] = $data->zusHealth[6] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '7':
                            $data->zusHealth[7] = $data->zusHealth[7] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '8':
                            $data->zusHealth[8] = $data->zusHealth[8] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '9':
                            $data->zusHealth[9] = $data->zusHealth[9] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '10':
                            $data->zusHealth[10] = $data->zusHealth[10] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                        case '11':
                            $data->zusHealth[11] = $data->zusHealth[11] + $bufElement['kwota'];
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];
                            break;
                       /* case '12':
                            $data->zusHealth[12] = $data->zusHealth[12] + $bufElement['kwota'];

                            break;*/
                    }

                }

                for ($i = 0; $i < 13; $i = $i + 1) {
                    $log->add(LOG_ALERT, "ZUS_Health: " . $data->zusHealth[$i]);
                }
            }

            /////////////ZUS_FGSP/////////////
            {
                $prevBuf = DB::select()->from($previousTableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 3)->where("strona", "=", 1)->execute($dbName);
                foreach ($prevBuf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '12':
                            $data->zusFpFgsp[0] = $data->zusFpFgsp[0] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                    }
                }

                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 3)->where("strona", "=", 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case '1':
                            $data->zusFpFgsp[1] = $data->zusFpFgsp[1] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '2':
                            $data->zusFpFgsp[2] = $data->zusFpFgsp[2] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '3':
                            $data->zusFpFgsp[3] = $data->zusFpFgsp[3] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '4':
                            $data->zusFpFgsp[4] = $data->zusFpFgsp[4] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '5':
                            $data->zusFpFgsp[5] = $data->zusFpFgsp[5] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '6':
                            $data->zusFpFgsp[6] = $data->zusFpFgsp[6] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '7':
                            $data->zusFpFgsp[7] = $data->zusFpFgsp[7] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '8':
                            $data->zusFpFgsp[8] = $data->zusFpFgsp[8] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '9':
                            $data->zusFpFgsp[9] = $data->zusFpFgsp[9] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '10':
                            $data->zusFpFgsp[10] = $data->zusFpFgsp[10] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
                        case '11':
                            $data->zusFpFgsp[11] = $data->zusFpFgsp[11] + $bufElement['kwota'];
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;
/*                        case '12':
                            $data->zusFpFgsp[12] = $data->zusFpFgsp[12] + $bufElement['kwota'];
                            break;*/
                    }

                }

                for ($i=0; $i<13;$i=$i+1) {
                    $log->add(LOG_ALERT,"ZUS_FGSP: ".$data->zusFpFgsp[$i]);
                }
            }

            /*/////////////VAT/////////////
                $data=$this->getVat($dbName, $data);
                $this->template->content = print_r($data->vatToPay);*/
            return $data;
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->sendMailWithError("getbuf    ".$e->getMessage());
        }
    }

    public function getConfig5($db)
    {
        try {$log = Log::instance();
            $return = NULL;
            unset($return);
            unset($dataq);
            /* $dataq = new Model_Config5($db);
                 $dataq = $dataq->find_all();*/

            $dataq = DB::select()->from("config5")->execute($db);

            foreach ($dataq as $key => $data):
                $return = $data['nazwaSkrocona'] . "<br>" . $data['ulica'] . " " . $data['dom'] . "<br>" . $data['kod'] . " " . $data['miejscowosc'];
                $log->add(Log::DEBUG, "ADRES!!       " . $return);
            endforeach;

            /*if($db=="15176927_klasteramfk")
                    $return="Klaster ICT";
                if($db=="15176927_wdmamfk")
                    $return="Wedding Day Media";*/


            $log->add(Log::ALERT, "5.getConfig:      " . $db . "<br> Adres:   " . $return);
            return $return;
        } catch (Exception $e) {
            $this->sendMailWithError("GetConfig5    ".$e->getMessage());
        }
    }

    public function get_taxOfficeAccount($db)
    {
        $data = DB::select()->from("urzad_sk")->execute($db);
        return $data[0]["rachunek"];
    }

    public function action_getRandString($length = 20, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count - 1)];
        }
        $Model = ORM::factory('EmailSchedules', 5);
        $Model->Link = $str;
        $Model->save();
        HTTP::redirect('app/index', 302);
    }

    public function getRandString($length, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count - 1)];
        }

        return $str;
    }

    public function action_checkDir()
    {
        try {
            $log = Log::instance();

            // echo shell_exec('unzip -P haselko DB/ntconsult.zip');
            //system('unzip -P haselko DB/ntconsult.zip');
            $zip = new ZipArchive();

            /*} else {
                    $zip->close();
                   // echo "Unable to unzip your zip archive.";
                }*/
            //$cos=zip_open('DB/1.rar') OR die('gsdfgsdfgsd');
            $files = scandir('DB');
            $count = count($files);
            if ($count > 2) {

                $files = glob('DB/*'); // get all file names
                foreach ($files as $file) { // iterate files
                    if (is_file($file))
                        $res = $zip->open($file) OR die('gsdfgsdfgsd');
                    //if ($res === TRUE) {

                    $zip->extractTo('DB') OR die('unable to unzip');
                    $zip->close();
                    /*$fileName=explode("/",$file);
                    copy($file, "backup/".date("c").$fileName[1]);*/
                    //$this->migrate($file);
                    // unlink($file); // delete file
                }
                $this->template->content = "coś było a teraz tego nie ma";
            } else {
                $this->template->content = "Pusto";
            }
            $dirs = scandir('DB');
            $count = count($dirs);
            if ($count > 2) { //jeśli jest jakiś plik w /DB/
                $dirs = glob('DB/*'); // get all file names
                foreach ($dirs as $dir) { // Katalog z nazwą biura księgowego
                    if (is_dir($dir)) { //Jeśli jest katalogiem
                        $companyName = explode("/", $dir);
                        $subDirs = glob('DB/' . $companyName[1] . "/*");
                        foreach ($subDirs as $subDir) { // iterate files
                            if (is_dir($subDir)) {
                                /*$database = explode("/", $subDir);
                                $log->add(Log::INFO, 'database0 ' . $database[0]);
                                $log->add(Log::INFO, 'database1 ' . $database[1]);
                                $log->add(Log::INFO, 'database2 ' . $database[2]);*/
                                $_firm = explode("/", $subDir);
                                $path = 'DB/' . $companyName[1] . "/" . $_firm[2] . "/*";
                                $log->add(Log::INFO, 'path   ' . $path);
                                $_subdirs = glob('DB/' . $companyName[1] . "/" . $_firm[2] . "/*");
                                foreach ($_subdirs as $_subDir) {
                                    if (is_dir($dir)) {
                                        $firmCode = explode("/", $_subDir);
                                        $log->add(Log::INFO, 'FirmCode ' . $firmCode[3]);
                                        $files = glob('DB/' . $companyName[1] . "/" . $_firm[2] . "/" . $firmCode[3] . "/*"); //Get all files
                                        foreach ($files as $filepath) {
                                            if (is_file($filepath)) {
                                                $log->add(Log::INFO, 'file ' . $filepath);
                                                $file = explode("/", $filepath);
                                                $log->add(Log::INFO, 'File 4  ' . $file[4]);
                                                try {
                                                    if (trim($file[4], "   .txt") == "zapisy_2013" OR trim($file[4], "   .txt") == "zapisy_2014" OR trim($file[4], "   .txt") == "zapisy_2015" OR trim($file[4], "   .txt") == "buf_zapisy_2013" OR trim($file[4], "   .txt") == "buf_zapisy_2014" OR trim($file[4], "   .txt") == "buf_zapisy_2015") {
                                                        $this->migrateDb($filepath, $file[4], $_firm[2], $firmCode[3]);
                                                    }
                                                } catch (Exception $ex) {
                                                    $log->add(Log::ERROR, $ex->getMessage());
                                                    //$tmp .= $ex->getMessage() . "//////////////" . $tableName . "-------" . $i . "txt///////////////////////////";
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                $files = scandir('DB');
                $count = count($files);
                if ($count > 2) {

                    $files = glob('DB/*'); // get all file names
                    foreach ($files as $file) { // iterate files
                        if (is_file($file)) {
                            unlink($file); // delete file
                        } else {
                            $this->delTree($file);
                        }

                    }
                }
                // $this->template->content = $tmp;
            }
        } catch (Exception $e) {
            $log->add(Log::ERROR, $e->getMessage());
            $this->sendMailWithError("action_checkDir   ".$e->getMessage());
        }
    }

    public function migrateDb($file, $tableName, $companyName, $databaseName)
    {
        $log = Log::instance();
        try {
            $log = Log::instance();
            $log->add(Log::INFO, 'File: ' . $file . '     Table: ' . trim($tableName, "   .txt") . '      Company:  ' . $companyName . '     DatabaseName:       ' . $databaseName);
            $time_start = $this->microtime_float();
            $query = DB::query(Database::DELETE, 'DELETE FROM ' . trim($tableName, "   .txt") . ';');
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $time_end =$this->microtime_float();
            $time = $time_end - $time_start;
            $log->add(LOG_INFO,'Truncate time: '.$time);
            $query = DB::query(Database::INSERT, ' LOAD DATA LOCAL INFILE \'' . $file . '\' INTO TABLE ' . trim($tableName, "   .txt") . ' FIELDS TERMINATED BY \',\';');
            $time_start = $this->microtime_float();
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $time_end = $this->microtime_float();
            $time = $time_end - $time_start;
            $log->add(LOG_INFO,'Load Data Infile Time: '.$time);
            $log->add(Log::NOTICE, "IMPORT OK");
        } catch (Exception $e) {
            $log->add(Log::ERROR, $e->getMessage());
            $this->sendMailWithError("MigrateDB ".$e->getMessage());
        }
    }

    public function migrate($file, $tableName, $companyName, $databaseName)
    {
        $log = Log::instance();
        try {
            $log = Log::instance();
            $log->add(Log::INFO, 'File: ' . $file . '     Table: ' . trim($tableName, "   .txt") . '      Company:  ' . $companyName . '     DatabaseName:       ' . $databaseName);
            $time_start = $this->microtime_float();
            $query=DB::query(Database::$default,'CREATE TABLE new_'.trim($tableName, "   .txt").' LIKE '.trim($tableName, "   .txt"));
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $query=DB::query(Database::$default,'RENAME TABLE '.trim($tableName, "   .txt").' TO old_'.trim($tableName, "   .txt").', new_'.trim($tableName, "   .txt").' TO '.trim($tableName, "   .txt"));
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $query=DB::query(Database::$default,'DROP TABLE old_'.trim($tableName, "   .txt"));
            //$query = DB::query(Database::DELETE, 'TRUNCATE TABLE ' . trim($tableName, "   .txt") . ';');
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $time_end =$this->microtime_float();
            $time = $time_end - $time_start;
            $log->add(LOG_INFO,'Truncate time: '.$time);
            $query = DB::query(Database::INSERT, ' LOAD DATA LOCAL INFILE \'' . $file . '\' INTO TABLE ' . trim($tableName, "   .txt") . ' FIELDS TERMINATED BY \',\';');
            $time_start = $this->microtime_float();
            $query->execute('15176927_' . $companyName . strtolower($databaseName));
            $time_end = $this->microtime_float();
            $time = $time_end - $time_start;
            $log->add(LOG_INFO,'Load Data Infile Time: '.$time);
            $log->add(Log::NOTICE, "IMPORT OK");
        } catch (Exception $e) {
            $log->add(Log::ERROR, $e->getMessage());            
            $this->sendMailWithError("Migrate   ".$e->getMessage());
        }
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    public function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public function action_delete()
    {
        if (Auth::instance()->logged_in()) {
            $Model = ORM::factory('EmailSchedules', $this->request->query('id'));
            $Model->delete();
            $uri = 'app/index?status=deleted';
            HTTP::redirect($uri, 302);
        } else {
            $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }
    }

    public function action_details()
    {
        if (Auth::instance()->logged_in()) {
            $Model = ORM::factory('EmailSchedules', $this->request->query('id'));

            $this->template->content = View::factory('app/details')
                ->set('model', $Model);
        } else {
            $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }

    }

    public function action_edit()
    {
        if (Auth::instance()->logged_in()) {
            $dbModel = ORM::factory("Databases")->where("UserId", "=", Auth::instance()->get_user()->id)->find_all();
            $dbm = array();
            for ($i = 0; $i < $dbModel->count(); $i++) {
                $dbm[$dbModel[$i]->Name] = $dbModel[$i]->Name;
            }
            $Model2 = ORM::factory('EmailSchedules', $this->request->query('id'));
            $post = Validation::factory($_POST);
            $post->rule('Email', 'not_empty')
                ->rule('FirmName', 'not_empty')
                ->rule('FirmName', 'regex', array(':value', "/^([a-zA-Z0-9,\\/.\"'\\s])+$/"))
                ->rule('Email', array('valid', 'email'))
                ->rule('DbName', 'not_empty')
                //->rule('week', 'not_empty')
                ->rule('PhoneNumber', array('valid', 'phone'))
                ->rule('PhoneNumber', 'not_empty');

            if ((((!isset($_POST['ZiK']) || $_POST['ZiK'] != "on")) && ((!isset($_POST['RZiS']) || ($_POST['RZiS'] != "on"))))) {
                $post->rule('ZiK', 'not_empty');
            }

            if ($this->request->post()) { //sprawdzenie czy przyszły dane z formularza
                if ($post->check()) {
                    $toSend = 0; //toSend - jakie dokumenty ma wysłać
                    if ((isset($_POST['RZiS'])) && $_POST['RZiS'] == "on") {
                        if ((isset($_POST['ZiK'])) && $_POST['ZiK'] == "on") {
                            $toSend = 3; //oba
                        } else {
                            $toSend = 1; //Rachunek zysków i strat
                        }

                    } else {
                        if (((isset($_POST['ZiK'])) && $_POST['ZiK'] == "on")) {
                            $toSend = 2; //Zysk i Koszty
                        }
                    }

                    $Mo = (isset($_POST['week']) && $_POST['week'] == "Mo") ? TRUE : FALSE; //Sprawdzanie dnia tygodnia do wysyłki
                    $Tu = (isset($_POST['week']) && $_POST['week'] == "Tu") ? TRUE : FALSE;
                    $We = (isset($_POST['week']) && $_POST['week'] == "We") ? TRUE : FALSE;
                    $Th = (isset($_POST['week']) && $_POST['week'] == "Th") ? TRUE : FALSE;
                    $Fr = (isset($_POST['week']) && $_POST['week'] == "Fr") ? TRUE : FALSE;
                    $Sa = (isset($_POST['week']) && $_POST['week'] == "Sa") ? TRUE : FALSE;
                    $Su = (isset($_POST['week']) && $_POST['week'] == "Su") ? TRUE : FALSE;

                    //$time=Date::formatted_time().;

                    // $this->template->content = View::factory('app/addClient')->bind('log', $tmp);
                    $Model2->PhoneNumber = $_POST['PhoneNumber'];
                    $Model2->Name = $_POST['FirmName']; //Dodanie danych do modelu
                    $Model2->Email = $_POST['Email'];
                    $Model2->DataBase = $_POST['DbName'];
                    $Model2->MemberID = Auth::instance()->get_user()->id;
                    $Model2->ScheduleID = $toSend;
                    $Model2->ScheduleName = $_POST['FirmName'] . "_" . $_POST['Email'];
                    $Model2->Mo = TRUE;
                    $Model2->Tu = FALSE;
                    $Model2->We = FALSE;
                    $Model2->Th = FALSE;
                    $Model2->Fr = FALSE;
                    $Model2->Sa = FALSE;
                    $Model2->Su = FALSE;
                    $Model2->Status = 1;
                    $Model2->CreateDate = Date::formatted_time();
                    $Model2->CreateEmail = "ldoszczeczko@gmail.com";


                    $Model2->save(); //Dodanie wpisu do bazy

                    unset($_POST); //Czyszczenie POST'a
                    //HTTP::redirect('app/index?status=addok', 302); //przekierowanie
                    $this->template->content = 'Edit';
                } else {
                    $errors = $post->errors('EmailSchedules');
                    $this->template->content = View::factory('app/edit')->bind('errors', $errors)->set('model', $Model2)->set('dbModel', $dbm);
                }
            } else {
                $errors = '';
                $this->template->content = View::factory('app/edit')->bind('errors', $errors)->set('model', $Model2)->set('dbModel', $dbm);
            }
        } else {
            $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }
    }

    public function action_temp()
    {
        $buf = '';
        $date = '';
        $data = ORM::factory('ReportData');
        $i = '';
        foreach ($data->incomeTab as $incomeTabElement) {
            //$buf = ORM::factory('Buf')->where('synt', '=', $incomeTabElement)->find_all();
            //$buf=Model::factory('Buf','15176927_ntcons');
            $name = "Model_Buf";
            $buf = new $name;
            //$buf->setDb('');
            $buf = $buf->where('synt', '=', $incomeTabElement)->find_all();
            foreach ($buf as $bufElement) {

            }

        }

        $this->template->content = print_r($bufElement['kwota']);

        /*if (Auth::instance()->logged_in()) {
            $this->sendMail('ldoszczeczko@gmail.com', 'fasdffdsfgsdfgsd');

        } else {
            $this->template->content = '<h2 style="color: red; text-align: center">Nie jesteś zalogowany!</h2>';
        }*/
        // $this->checkEmailSchedules();
    }

    public function action_test()
    {
        //$this->executeSmsSchedules('dfasdfasd');
        //$this->createpdf('15176927_busaccoamfk10000', 1);
        // $this->template->content = $this->getVatToPay();
        $functions=new Functions();
        echo date("r");
        //print_r($this->getVat("15176927_busaccoamfk10000", ORM::factory('ReportData'), "2013", "zapisy_2013"));
        //$this->getCompanyNameFromDb("15176927_klasteramfk");
        //$buf = DB::select()->from("zapisy_2013")->where("synt", "=", 220)->execute("15176927_busaccoamfk10000");
    }

    public function action_new()
    {
        $cit = '';
        $buf = DB::select()->from('buf_zapisy_2014')->where('synt', '=', 871)->where('strona', '=', 0)->execute('15176927_ntconsultamfk10001');
        foreach ($buf as $bufElement) {
            if ($bufElement['opis'] == " CIT I/2014") {
                echo $bufElement['kwota'];
            }
        }
        //$this->template->scripts = array('assets/js/jqtest.js');


        //$this->template->content= date("m");
        /* // Create a array
         $stack = array();

 //Iniciate Miltiple Thread
         foreach ( range("A", "D") as $i ) {
             $stack[$i] = new Model_SetSmsSchedulesAsync($i);

         }

 // Start The Threads
         foreach ( $stack as $t ) {
             $t->start();
         }*/
    }

    public function sendSmsWithIncomesCostsAndTotal($currentMonth)
    {
        try {
            $smsSchedules = DB::select()->from('smsschedules')->where('type', '=', 1)->execute();

            foreach ($smsSchedules as $sms) {
                $phoneNumber = $sms['phoneNumber'];
                $cost = $sms['cost'];
                $income = $sms['income'];
                $total = $sms['total'];
                $pit=$sms['pit-4'];
                $message = $currentMonth . '- Przychód: ' . $income . ' | Koszty: ' . $cost . ' | Dochód: ' . $total.' | PIT-4: '.$pit;
                $this->sendsms($phoneNumber, $message);
               /* $Model = ORM::factory('smsschedules', $sms->id);
                $Model->delete();*/
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function sendSmsWithCit($currentMonth)
    {
        try {
            $smsSchedules = DB::select()->from('smsschedules')->where('type', '=', 2)->execute();

            foreach ($smsSchedules as $sms) {
                $phoneNumber = $sms['phoneNumber'];
                $cit = $sms['cit'];
                $message = $currentMonth . '- CIT: ' . $cit;
                $this->sendsms($phoneNumber, $message);
                $Model = ORM::factory('smsschedules', $sms->id);
                $Model->delete();
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function sendSmsWithPit($currentMonth)
    {
        try {
            $smsSchedules = DB::select()->from('smsschedules')->where('type', '=', 3)->execute();

            foreach ($smsSchedules as $sms) {
                $phoneNumber = $sms['phoneNumber'];
                $pit = $sms['pit-4'];
                $message = $currentMonth . '- PIT-4: ' . $pit;
                $this->sendsms($phoneNumber, $message);
                $Model = ORM::factory('smsschedules', $sms->id);
                $Model->delete();
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function sendSmsWithVat($currentMonth)
    {
        try {
            $smsSchedules = DB::select()->from('smsschedules')->where('type', '=', 5)->execute();

            foreach ($smsSchedules as $sms) {
                $phoneNumber = $sms['phoneNumber'];
                $vat = $sms['vat'];
                $message = $currentMonth.'- VAT do zapłaty: ' . $vat;
                $this->sendsms($phoneNumber, $message);
                $Model = ORM::factory('smsschedules', $sms->id);
                $Model->delete();
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function sendSmsWithZus($currentMonth)
    {
        try {
            $smsSchedules = DB::select()->from('smsschedules')->where('type', '=', 4)->execute();

            foreach ($smsSchedules as $sms) {
                $phoneNumber = $sms['phoneNumber'];
                $zusSocial = $sms['zusSocial'];
                $zusHealth = $sms['zusHealth'];
                $zusFgsp = $sms['zusFgsp'];
                $message = $currentMonth . '- ZUS ub. Socjalne: ' . $zusSocial . ' | ZUS ub. Zdrowotne: ' . $zusHealth . ' | ZUS FP i FGSP: ' . $zusFgsp;
                $this->sendsms($phoneNumber, $message);
                /*$Model = ORM::factory('smsschedules', $sms->id);
                $Model->delete();*/
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
            $this->sendMailWithError("SMS     ".$e->getMessage());
        }
    }

    public function sendsms($phoneNumber, $message)
    {
        require_once 'modules/smsapi/Autoload.php';

        $client = new \SMSApi\Client('e-raport.pl');
        $client->setPasswordHash(md5('jN2ycyf1ga$t'));

        $smsapi = new \SMSApi\Api\SmsFactory();
        $smsapi->setClient($client);
        $log = Log::instance();

        try {

            $actionSend = $smsapi->actionSend();

            $actionSend->setTo($phoneNumber);
            $actionSend->setText($message);
            $actionSend->setSender('e-raport.pl'); //Pole nadawcy lub typ wiadomość 'ECO', '2Way'

            $response = $actionSend->execute();

            foreach ($response->getList() as $status) {
                echo $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();

                $log->add(Log::INFO, "SEND SMS:        " . $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus());
            }
        } catch (\SMSApi\Exception\SmsapiException $e) {
            echo 'ERROR: ' . $e->getMessage();
            $this->sendMailWithError("SEND SMS      ".$e->getMessage());
        }

    }

    public function action_setSmsSchedules()
    {
        $functions = new Functions();
        $currentDayOfMonth = $functions->getCurrentDayOfMonth();
        $report_data = ORM::factory('ReportData');
        $emailSchedulesModel = ORM::factory('EmailSchedules')->find_all();
        foreach ($emailSchedulesModel as $key => $emailSchedule) {
            $dbModel = ORM::factory('Databases')->where("Name", "=", $emailSchedule->DataBase)->find();

            switch ($currentDayOfMonth) {
                case '9':
                    try {
                        $this->getIncomesCostsTotalAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                        $this->getPitAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                        break;
                    } catch (Exception $ex) {
                        $this->template->content = $ex->getMessage();
                    };
                case '14':
                    $this->getZusAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                    break;

                case '20':
                    $this->getCitAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                    $this->getPitAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                    $this->getIncomesCostsTotalAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                    break;

                case '11':
                    $this->getVatAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
                    break;
            }

            //$this->getIncomesCostsTotalAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
            //$this->getPitAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);

            //$this->getZusAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);

            //$this->getCitAndInsertIntoDatabase($report_data, $dbModel->FullName, $emailSchedule->PhoneNumber, $dbModel->CompanyName, $emailSchedule->id);
        }


    }

    public function getCitAndInsertIntoDatabase($data, $dbName, $phoneNumber, $companyName, $emailScheduleId)
    {
        $functions=new Functions();
        $log=Log::instance();
        //$currentMonth=($functions->getCurrentIntMonth()<10)?"0".$functions->getCurrentIntMonth():$functions->getCurrentIntMonth();
        $currentMonth=12;
        $log->add(LOG_INFO,"CIT current Month: ".$currentMonth);
        $cit = 0;
        $tableName = null;

        $tableName = null;
        for ($i = 0; $i < 2; $i = $i + 1) {
            if ($i == 0) {
                $tableName = "zapisy_2014";
            } else {
                $tableName = "buf_zapisy_2014";
            }
            /////////////CIT/////////////

            $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1','=',1)->where('strona', '=', 1)->execute($dbName);
            foreach ($buf as $bufElement) {
                $citDescription = explode(" ", trim($bufElement['opis']));
                $isCit = $citDescription[0];
                $citDescriptionDate = $citDescription[1];
                if ($isCit == "CIT") {
                    $citDate = explode("/", $citDescriptionDate);
                    $month = $citDate[0];
                    //$log->add(LOG_INFO, "CIT Description:  " . $citDescription[0] . "+   " . $citDescription[1] . "+   value:  " . $bufElement['kwota']);
                    switch ($month) {
                        case $currentMonth:
                            $cit = $cit + $bufElement['kwota'];
                            break;
                    }
                }
            }
            /*$j = 0;
            foreach ($buf as $bufElement) {
                $j += 1;
                if ($j == $currentDate) {
                    $cit = $cit + $bufElement['kwota'];
                }
            }*/

        }

        $query = "INSERT INTO smsschedules VALUES ('',$emailScheduleId, $phoneNumber, '$companyName', 2, '', '' ,'' ,$cit,'','','','','','',1)";
        $smsSchedules = DB::query(Database::INSERT, $query)->execute();
        // $this->sendsms($phoneNumber, $income, $cost, $total);
    }

    public function getIncomesCostsTotalAndInsertIntoDatabase($data, $dbName, $phoneNumber, $companyName, $emailScheduleId)
    {
        $currentDate = 12;
        $income = 0;
        $cost = Null;
        $total = null;
        $tableName = null;
        for ($i = 0; $i < 2; $i = $i + 1) {
            if ($i == 0) {
                $tableName = "zapisy_2014";
            } else {
                $tableName = "buf_zapisy_2014";
            }
            foreach ($data->incomeTab as $incomeTabElement) {
                //$buf = ORM::factory($tableName)->where('synt', '=', $incomeTabElement)->find_all();

                $bufDec = DB::select()->from($tableName)->where('synt', '=', $incomeTabElement)->where('strona', '=', 1)->execute($dbName);

                $buf = DB::select()->from($tableName)->where('synt', '=', $incomeTabElement)->execute($dbName);

                /* $name = "Model_" . $tableName;
                 $buf = new $name($dbName);
                 $buf = $buf->where('synt', '=', $incomeTabElement)->find_all();*/


                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case  $currentDate:
                            $income = $income + $bufElement['kwota'];
                            break;
                    }
                }
            }
            foreach ($data->costTab as $costTabElement) {
                if ($costTabElement == 755) {
                    $val1 = null;
                    $val2 = null;
                    $buf = DB::select()->from($tableName)->where('synt', '=', $costTabElement)->where('poz1', '=', 1)->execute($dbName);
                    foreach ($buf as $bufElement) {
                        $date = $bufElement['dataokr'];
                        $date = explode('-', $date);
                        switch ($date[1]) {
                            case  $currentDate:
                                $val1 = $val1 + $bufElement['kwota'];
                                break;
                        }
                    }
                    $buf = DB::select()->from($tableName)->where('synt', '=', $costTabElement)->where('poz1', '=', 2)->execute($dbName);
                    foreach ($buf as $bufElement) {
                        $date = $bufElement['dataokr'];
                        $date = explode('-', $date);
                        switch ($date[1]) {
                            case  $currentDate:
                                $val2 = $val2 + $bufElement['kwota'];
                                break;
                        }
                    }
                    $cost = $cost + ($val2 - $val1);

                } else {
                    $buf = DB::select()->from($tableName)->where('synt', '=', $costTabElement)->execute($dbName);
                    foreach ($buf as $bufElement) {
                        $date = $bufElement['dataokr'];
                        $date = explode('-', $date);
                        switch ($date[1]) {
                            case  $currentDate:
                                $cost = $cost + $bufElement['kwota'];
                                break;

                        }

                    }
                }
            }
        }
        $total = $income - $cost;

        $query = "INSERT INTO smsschedules VALUES ('', $emailScheduleId, $phoneNumber, '$companyName', 1, $income, $cost,$total,'','','','','','','',1)";
        $smsSchedules = DB::query(Database::INSERT, $query)->execute();
        // $this->sendsms($phoneNumber, $income, $cost, $total);
    }

    public function getPitAndInsertIntoDatabase($data, $dbName, $phoneNumber, $companyName, $emailScheduleId)
    {
        $currentDate = 12;
        $pit = 0;
        $tableName = null;
        for ($i = 0; $i < 2; $i = $i + 1) {
            if ($i == 0) {
                $tableName = "zapisy_2014";
            } else {
                $tableName = "buf_zapisy_2014";
            }
            {
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 2)->where('strona', '=', 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case $currentDate - 1:
                            $pit = $pit + $bufElement['kwota'];
                            break;
                    }
                }
            }
        }


        $query = "INSERT INTO smsschedules VALUES ('',  $emailScheduleId, $phoneNumber, '$companyName', 3, '', '','','',$pit,'','','','','',1)";
        $smsSchedules = DB::query(Database::INSERT, $query)->execute();
    }

    public function getZusAndInsertIntoDatabase($data, $dbName, $phoneNumber, $companyName, $emailScheduleId)
    {
        $currentDate = 11;
        $zusSocial = 0;
        $zusHealth = 0;
        $zusFgsp = 0;
        $tableName = null;
        for ($i = 0; $i < 2; $i = $i + 1) {
            if ($i == 0) {
                $tableName = "zapisy_2014";
            } else {
                $tableName = "buf_zapisy_2014";
            }
            /////////////ZUS_SOCIAL/////////////
            {
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 1)->where("strona","=",1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case $currentDate:
                            $zusSocial = $zusSocial + $bufElement['kwota'];
                            break;
                    }
                }
            }

            /////////////ZUS_HEALTH/////////////
            {
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 2)->where("strona","=",1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case $currentDate:
                            $zusHealth = $zusHealth + $bufElement['kwota'];
                            break;
                    }
                }
            }

            /////////////ZUS_FGSP/////////////
            {
                $buf = DB::select()->from($tableName)->where('synt', '=', 220)->where('poz1', '=', 3)->where('poz2', '=', 3)->where("strona","=",1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    switch ($date[1]) {
                        case $currentDate:
                            $zusFgsp = $zusFgsp + $bufElement['kwota'];
                            break;
                    }
                }
            }
        }


        $query = "INSERT INTO smsschedules VALUES ('',$emailScheduleId, $phoneNumber, '$companyName', 4, '', '','','','',$zusSocial,$zusHealth,$zusFgsp,'','',1)";
        $smsSchedules = DB::query(Database::INSERT, $query)->execute();
    }

    /*    public function getVat1($dbName, $year, $data)
        {
            /////////////VAT_TO_PAY/////////////
            {
                $dSkrot = "dSkrot";
                $fvs = "FVS";
                //$buf = DB::select()->from("REJVAT")->where($dSkrot, "=", $fvs)->execute("15176927_busaccoamfk10000");
                $buf = DB::select()->from("REJVAT")->where("def", "=", 5)->execute($dbName);
                $ret = ORM::factory('ReportData');
                foreach ($buf as $bufElement) {
                    $date = $bufElement['okres'];
                    $date = explode('-', $date);
                    if ($date[0] == $year) {
                        switch ($date[1]) {
                            case '1':
                                $data->vat1[0] = $data->vat1[0] + $bufElement['vat'];
                                break;
                            case '2':
                                $data->vat1[1] = $data->vat1[1] + $bufElement['vat'];
                                break;
                            case '3':
                                $data->vat1[2] = $data->vat1[2] + $bufElement['vat'];
                                break;
                            case '4':
                                $data->vat1[3] = $data->vat1[3] + $bufElement['vat'];
                                break;
                            case '5':
                                $data->vat1[4] = $data->vat1[4] + $bufElement['vat'];
                                break;
                            case '6':
                                $data->vat1[5] = $data->vat1[5] + $bufElement['vat'];
                                break;
                            case '7':
                                $data->vat1[6] = $data->vat1[6] + $bufElement['vat'];
                                break;
                            case '8':
                                $data->vat1[7] = $data->vat1[7] + $bufElement['vat'];
                                break;
                            case '9':
                                $data->vat1[8] = $data->vat1[8] + $bufElement['vat'];
                                break;
                            case '10':
                                $data->vat1[9] = $data->vat1[9] + $bufElement['vat'];
                                break;
                            case '11':
                                $data->vat1[10] = $data->vat1[10] + $bufElement['vat'];
                                break;
                            case '12':
                                $data->vat1[11] = $data->vat1[11] + $bufElement['vat'];
                                break;
                        }

                        $data->vat1[12] = $data->vat1[12] + $bufElement['vat'];
                        $ret = $date;
                    }
                }
            }
            return $data->vat1;
        }

        public function getVat2($dbName, $year, $data)
        {
            /////////////VAT_TO_RETURN/////////////
            {
                $dSkrot = "dSkrot";
                $fvs = "FVS";
                //$buf = DB::select()->from("REJVAT")->where($dSkrot, "=", $fvs)->execute("15176927_busaccoamfk10000");
                $buf = DB::select()->from("REJVAT")->where("def", "=", 4)->execute($dbName);

                foreach ($buf as $bufElement) {
                    $date = $bufElement['okres'];
                    $date = explode('-', $date);
                    if ($date[0] == $year) {
                        switch ($date[1]) {
                            case '1':
                                $data->vat2[0] = $data->vat2[0] + $bufElement['vat'];
                                break;
                            case '2':
                                $data->vat2[1] = $data->vat2[1] + $bufElement['vat'];
                                break;
                            case '3':
                                $data->vat2[2] = $data->vat2[2] + $bufElement['vat'];
                                break;
                            case '4':
                                $data->vat2[3] = $data->vat2[3] + $bufElement['vat'];
                                break;
                            case '5':
                                $data->vat2[4] = $data->vat2[4] + $bufElement['vat'];
                                break;
                            case '6':
                                $data->vat2[5] = $data->vat2[5] + $bufElement['vat'];
                                break;
                            case '7':
                                $data->vat2[6] = $data->vat2[6] + $bufElement['vat'];
                                break;
                            case '8':
                                $data->vat2[7] = $data->vat2[7] + $bufElement['vat'];
                                break;
                            case '9':
                                $data->vat2[8] = $data->vat2[8] + $bufElement['vat'];
                                break;
                            case '10':
                                $data->vat2[9] = $data->vat2[9] + $bufElement['vat'];
                                break;
                            case '11':
                                $data->vat2[10] = $data->vat2[10] + $bufElement['vat'];
                                break;
                            case '12':
                                $data->vat2[11] = $data->vat2[11] + $bufElement['vat'];
                                break;
                        }

                        $data->vat2[12] = $data->vat2[12] + $bufElement['vat'];
                        $ret = $date;
                    }
                }
            }
            return $data->vat2;
        }

        public function getVat1new($dbName, $year, $data)
        { //ToDo: zamienić 1=0
            /////////////VAT_TO_PAY/////////////
            {
                $dSkrot = "dSkrot";
                $fvs = "FVS";
                //$buf = DB::select()->from("REJVAT")->where($dSkrot, "=", $fvs)->execute("15176927_busaccoamfk10000");
                $buf = DB::select()->from("buf_zapisy_2014")->where("synt", "=", 221)->where("poz1", "=", 5)->where("strona", "=", 0)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    if ($date[0] == "2014" AND $date[1] > "9") {
                        if ($date[0] == $year) {
                            switch ($date[1]) {
                                case '1':
                                    $data->vat1[0] = $data->vat1[0] + $bufElement['kwota'];
                                    break;
                                case '2':
                                    $data->vat1[1] = $data->vat1[1] + $bufElement['kwota'];
                                    break;
                                case '3':
                                    $data->vat1[2] = $data->vat1[2] + $bufElement['kwota'];
                                    break;
                                case '4':
                                    $data->vat1[3] = $data->vat1[3] + $bufElement['kwota'];
                                    break;
                                case '5':
                                    $data->vat1[4] = $data->vat1[4] + $bufElement['kwota'];
                                    break;
                                case '6':
                                    $data->vat1[5] = $data->vat1[5] + $bufElement['kwota'];
                                    break;
                                case '7':
                                    $data->vat1[6] = $data->vat1[6] + $bufElement['kwota'];
                                    break;
                                case '8':
                                    $data->vat1[7] = $data->vat1[7] + $bufElement['kwota'];
                                    break;
                                case '9':
                                    $data->vat1[8] = $data->vat1[8] + $bufElement['kwota'];
                                    break;
                                case '10':
                                    $data->vat1[9] = $data->vat1[9] + $bufElement['kwota'];
                                    break;
                                case '11':
                                    $data->vat1[10] = $data->vat1[10] + $bufElement['kwota'];
                                    break;
                                case '12':
                                    $data->vat1[11] = $data->vat1[11] + $bufElement['kwota'];
                                    break;
                            }

                            $data->vat1[12] = $data->vat1[12] + $bufElement['kwota'];
                        }
                    }
                }
            }
            return $data->vat1;
        }

        public function getVat2new($dbName, $year, $data)
        {
            /////////////VAT_TO_RETURN/////////////
            {
                $dSkrot = "dSkrot";
                $fvs = "FVS";
                //$buf = DB::select()->from("REJVAT")->where($dSkrot, "=", $fvs)->execute("15176927_busaccoamfk10000");

                $buf = DB::select()->from('buf_zapisy_2014')->where("synt", "=", 221)->where("poz1", "=", 5)->where("strona", "=", 1)->execute($dbName);
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    if ($date[0] == "2014" AND $date[1] > "9") {
                        if ($date[0] == $year) {
                            switch ($date[1]) {
                                case '1':
                                    $data->vat2[0] = $data->vat2[0] + $bufElement['kwota'];
                                    break;
                                case '2':
                                    $data->vat2[1] = $data->vat2[1] + $bufElement['kwota'];
                                    break;
                                case '3':
                                    $data->vat2[2] = $data->vat2[2] + $bufElement['kwota'];
                                    break;
                                case '4':
                                    $data->vat2[3] = $data->vat2[3] + $bufElement['kwota'];
                                    break;
                                case '5':
                                    $data->vat2[4] = $data->vat2[4] + $bufElement['kwota'];
                                    break;
                                case '6':
                                    $data->vat2[5] = $data->vat2[5] + $bufElement['kwota'];
                                    break;
                                case '7':
                                    $data->vat2[6] = $data->vat2[6] + $bufElement['kwota'];
                                    break;
                                case '8':
                                    $data->vat2[7] = $data->vat2[7] + $bufElement['kwota'];
                                    break;
                                case '9':
                                    $data->vat2[8] = $data->vat2[8] + $bufElement['kwota'];
                                    break;
                                case '10':
                                    $data->vat2[9] = $data->vat2[9] + $bufElement['kwota'];
                                    break;
                                case '11':
                                    $data->vat2[10] = $data->vat2[10] + $bufElement['kwota'];
                                    break;
                                case '12':
                                    $data->vat2[11] = $data->vat2[11] + $bufElement['kwota'];
                                    break;
                            }

                            $data->vat2[12] = $data->vat2[12] + $bufElement['kwota'];
                            $ret = $date;
                        }
                    }
                }
            }
            return $data->vat2;
        }*/

    public function getVat1($dbName, $year, $table)
    {

        $vat1 = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        /////////////VAT_TO_PAY/////////////
        {

            $log = Log::instance();
            try {
                $dSkrot = "dSkrot";
                $fvs = "FVS";
                //$buf = DB::select()->from("REJVAT")->where($dSkrot, "=", $fvs)->execute("15176927_busaccoamfk10000");
                $buf = DB::select()->from($table)->where('synt', '=', 221)->where("poz1","=", 1)->where("strona","=",0)->execute($dbName);
                $ret = ORM::factory('ReportData');
                foreach ($buf as $bufElement) {
                    $date = $bufElement['dataokr'];
                    $date = explode('-', $date);
                    if ($date[0] == $year) {
                        switch ($date[1]) {
                            case '1':
                                $vat1[0] = $vat1[0] + $bufElement['kwota'];
                                break;
                            case '2':
                                $vat1[1] = $vat1[1] + $bufElement['kwota'];
                                break;
                            case '3':
                                $vat1[2] = $vat1[2] + $bufElement['kwota'];
                                break;
                            case '4':
                                $vat1[3] = $vat1[3] + $bufElement['kwota'];
                                break;
                            case '5':
                                $vat1[4] = $vat1[4] + $bufElement['kwota'];
                                break;
                            case '6':
                                $vat1[5] = $vat1[5] + $bufElement['kwota'];
                                break;
                            case '7':
                                $vat1[6] = $vat1[6] + $bufElement['kwota'];
                                break;
                            case '8':
                                $vat1[7] = $vat1[7] + $bufElement['kwota'];
                                break;
                            case '9':
                                $vat1[8] = $vat1[8] + $bufElement['kwota'];
                                break;
                            case '10':
                                $vat1[9] = $vat1[9] + $bufElement['kwota'];
                                break;
                            case '11':
                                $vat1[10] = $vat1[10] + $bufElement['kwota'];
                                break;
                            case '12':
                                $vat1[11] = $vat1[11] + $bufElement['kwota'];
                                break;
                        }

                        $vat1[12] = $vat1[12] + $bufElement['kwota'];
                        $ret = $date;
                    }
                }
            } catch (Exception $e) {

                $log->add(Log::ALERT, "exception:        " . $e->getMessage());
            }
        }
        return $vat1;
    }

    public function getVat2($dbName, $year, $table)
    {
        /////////////VAT_TO_get/////////////
        {
            $log = Log::instance();
            $vat1 = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            $buf = DB::select()->from($table)->where("synt", "=", 221)->where("poz1", "=", 2)->where("strona","=",1)->execute($dbName);
            $ret = ORM::factory('ReportData');
            foreach ($buf as $bufElement) {
                $date = $bufElement['dataokr'];
                $date = explode('-', $date);
                if ($date[0] == $year) {
                    switch ($date[1]) {
                        case '1':
                            $vat1[0] = $vat1[0] + $bufElement['kwota'];
                            break;
                        case '2':
                            $vat1[1] = $vat1[1] + $bufElement['kwota'];
                            break;
                        case '3':
                            $vat1[2] = $vat1[2] + $bufElement['kwota'];
                            break;
                        case '4':
                            $vat1[3] = $vat1[3] + $bufElement['kwota'];
                            break;
                        case '5':
                            $vat1[4] = $vat1[4] + $bufElement['kwota'];
                            break;
                        case '6':
                            $vat1[5] = $vat1[5] + $bufElement['kwota'];
                            break;
                        case '7':
                            $vat1[6] = $vat1[6] + $bufElement['kwota'];
                            break;
                        case '8':
                            $vat1[7] = $vat1[7] + $bufElement['kwota'];
                            break;
                        case '9':
                            $vat1[8] = $vat1[8] + $bufElement['kwota'];
                            break;
                        case '10':
                            $vat1[9] = $vat1[9] + $bufElement['kwota'];
                            break;
                        case '11':
                            $vat1[10] = $vat1[10] + $bufElement['kwota'];
                            break;
                        case '12':
                            $vat1[11] = $vat1[11] + $bufElement['kwota'];
                            break;
                    }

                    $vat1[12] = $vat1[12] + $bufElement['kwota'];
                }
            }
        }
        return $vat1;
    }

    public function getVat($dbName, $data, $year, $table)
    {/*
        $vat1 = $this->getVat1($dbName, $year, $data, "zapisy_2013");
        $vat2 = $this->getVat2($dbName, $year, $data, "zapisy_2013");*/

        $vat1 = $this->getVat1($dbName, $year, $table);
        $vat2 = $this->getVat2($dbName, $year, $table);
        $log = Log::instance();

        for ($i = 0; $i < 13; $i = $i + 1) {
            $log->add(Log::ALERT, "VAT1:        " . $vat1[$i]);
            $log->add(Log::ALERT, "VAT2:        " . $vat2[$i]);
            if ($vat1[$i] < $vat2[$i]) {
                //TODO:przeniesienie
                $data->vatToPay[$i] = 0;
                $data->vatToReturn[$i]=$vat1[$i]-$vat2[$i];
            } else {
                $data->vatToPay[$i] = $vat2[$i] - $vat1[$i];
                $data->vatToReturn[$i]=0;
            }
        }
        $this->template->content = print_r($dbName);
        return $data;
    }

    Public Function getVatAndInsertIntoDatabase($data, $dbName, $phoneNumber, $companyName, $emailScheduleId)
    {
        $log = Log::instance();
        $functions = new Functions();
        $vat = '';
        $data = ORM::factory('ReportData');
        $currentYear=$functions->getCurrentYear();
        //$data = $this->getVat($dbName, $data, $functions->getCurrentYear(), "zapisy_".$functions->getCurrentYear());
        $data = $this->getVat($dbName, $data, "2013", "zapisy_2013");
        //$currentMonthInt = $functions->getCurrentIntMonth() - 2;
        $currentMonthInt = 11;
        $vat = $data->vatToPay[$currentMonthInt];
        $vatToReturn=$data->vatToReturn[$currentMonthInt];
        $query = "INSERT INTO smsschedules VALUES ('',$emailScheduleId, $phoneNumber, '$companyName', 5, '', '','','','','','','',$vat,$vatToReturn,1)";
        $smsSchedules = DB::query(Database::INSERT, $query)->execute();
        $log->add(LOG_INFO,"Current Month: ".$functions->getCurrentIntMonth());
        $log->add(LOG_INFO,"Current Year: ".$currentYear);
        $log->add(LOG_INFO,"Database: ".$dbName);
        $log->add(LOG_INFO,"Company Name: ".$companyName);
    }

}