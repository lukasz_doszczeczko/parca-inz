<?php defined('SYSPATH') OR die('No Direct Script Access');

Class Model_ReportData extends Model
{
    public $income=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $incomeTab=array(702,762,763,751,752,754,756,701,704,755);

    public $cost=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $costTab=array(401,402,403,404,405,406,407,408,409,767,764,757,758,755,759,755);

    public $total=array(0,0,0,0,0,0,0,0,0,0,0,0,0);

    public $cit2get=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $cit2pay=array(0,0,0,0,0,0,0,0,0,0,0,0,0);

    public $pit2get=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $pit2pay=array(0,0,0,0,0,0,0,0,0,0,0,0,0);

    public $zusSocial=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $zusHealth=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $zusFpFgsp=array(0,0,0,0,0,0,0,0,0,0,0,0,0);


    //ToDO: Zmienić nazwy!
    public $vat1=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $vat2=array(0,0,0,0,0,0,0,0,0,0,0,0,0);

    public $vatToReturn=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
    public $vatToPay=array(0,0,0,0,0,0,0,0,0,0,0,0,0);
}

?>
