﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using inz.inzdbDataSetTableAdapters;
using inz.Properties;

namespace inz
{
    public enum SecurityMethods
    {
        Substitution,
        Delete,
        ChangePrivileges
    }

    public partial class MainForm : Form
    {
        //TODO: zrobić coś z tą zmienną
        string _fileSize;
        FileSystemWatcher _watcher;
        private bool _loggedin=false;
        static int _runningThreads;
        private const int MaxThreads = 100;
        private BackgroundWorker[] threadArray = new BackgroundWorker[MaxThreads];
        private User user;
        readonly string _currentUser = Environment.UserDomainName + "\\" + Environment.UserName;
        private ClipboardManager Manager;
        private readonly FilesManager _filesManager;
        private readonly DatabaseManager _databaseManager = new DatabaseManager();
        public delegate void DeleteFileDelegate(string path);
        public delegate void ChangeePrivilegesDelegate(string path);
        public delegate void ReplaceFileDelegate(string path);

        public delegate void MarkFileAsSafeDelegate(string path);

        public MarkFileAsSafeDelegate MarkFileAsSafe;
        public ReplaceFileDelegate ReplaceFile; 
        public DeleteFileDelegate DeleteFile;
        public ChangeePrivilegesDelegate ChangeePrivileges;

        public MainForm()
        {
            InitializeComponent();
            InitializeBackgoundWorkers();
            Watch();
            Manager = new ClipboardManager(this, _currentUser);
            _filesManager = new FilesManager(this, _currentUser);

            //ToDo: Delegat do Logów
            Manager.OnNewFilesFound += (sender, eventArg) =>
            {
                foreach (String item in eventArg)
                {
                    string fileMd5 = CreateMd5ForFile(item);
                    var itemSplited = item.Split('\\');
                    var name = itemSplited[itemSplited.Count() - 1];
                    var fileIsRestricted = _databaseManager.CheckIfHashAndNameExist(fileMd5, name);
                    if (fileIsRestricted != null)
                    {
                        //Clipboard.Clear();
                    }
                }
            };

            DeleteFile = _filesManager.DeleteFile;
            ChangeePrivileges = _filesManager.ChangeFilePrivileges;
            ReplaceFile = _filesManager.SubstituteFile;
            MarkFileAsSafe = _databaseManager.MarkFileAsSafe;
            Manager.StartChecking();

            SetToolStripButtonBold(Settings.Default.SecuritySetting);
        }

        public MainForm(User user)
        {
            InitializeComponent();
            InitializeBackgoundWorkers();
            Watch();
            this.user = user;
        }

        private void InitializeBackgoundWorkers()
        {
            for (var f = 0; f < MaxThreads; f++)
            {
                threadArray[f] = new BackgroundWorker();
                threadArray[f].RunWorkerCompleted += backgroundWorker2_RunWorkerCompleted;
                threadArray[f].ProgressChanged += backgroundWorker2_ProgressChanged;
                threadArray[f].WorkerReportsProgress = true;
                threadArray[f].WorkerSupportsCancellation = true;
                threadArray[f].DoWork += backgroundWorker2_DoWork;
            }
        }

        private int FindFreeBackGroundWorker()
        {

            while (true)
            {
                try
                {
                    for (var i = 0; i < MaxThreads; i++)
                    {
                        if (!threadArray[i].IsBusy)
                        {
                            return i;
                        }
                        Console.WriteLine("Wątek zajęty: " + i);
                    }
                    Thread.Sleep(500);
                }
                catch (Exception ex)
                {
                    Log.Error("FindFreeBackGroundWorker:    " + ex.Message);
                }
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'inzdbDataSet.hashes' table. You can move, or remove it, as needed.
            //TODO try
            hashesTableAdapter.Fill(inzdbDataSet.hashes);

        }

        private void Watch()
        {
            try
            {
                _watcher = new FileSystemWatcher
                    {
                        //Path = "C:\\Users\\Kubus\\Desktop\\",
                        Path = "C:\\",
                        NotifyFilter = NotifyFilters.Size,
                        IncludeSubdirectories = true,
                        Filter = "*.*"
                    };
                //| NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.Security;
                _watcher.Changed += OnWatcherChanged;
                _watcher.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                Log.Error("Watch:   " + ex.Message);
            }
        }

        private void OnWatcherChanged(object source, FileSystemEventArgs e)
        {
            //ToDo: log that new file was detected         
            try
            {
                var file = new FileInfo(e.FullPath);
                var fileAccessControl = file.GetAccessControl();
                var owner = fileAccessControl.GetOwner(typeof(NTAccount));

                if (_currentUser.Equals(owner.ToString()))
                {
                    //ToDo: Zmienna wielkość pliku lokalna? usunąć globalną
                    if (file.Length.ToString().Equals(_fileSize))
                    {
                        Console.WriteLine(@"Znaleziono plik " + file.FullName);
                        var filePath = e.FullPath;
                        var freeBackgroundWorkerIndex = FindFreeBackGroundWorker();
                        //check if file is restricted and secure it
                        threadArray[freeBackgroundWorkerIndex].RunWorkerAsync(filePath + "/" + file.Name);

                    }
                    else
                    {
                        _fileSize = file.Length.ToString();
                        Thread.Sleep(500);
                        OnWatcherChanged(source, e);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log.Error("OnWatcherChanged:    " + ex.Message);
                // System.Threading.Thread.Sleep(500);
                //ToDo: check if file still exist
                // OnChanged(source, e);
            }
        }

        private void tsbDodaj_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() != DialogResult.OK) return;
            var returnedPath = folderBrowserDialog1.SelectedPath;
            CheckDirAndInsertToDb(returnedPath);
        }

        private void CheckDirAndInsertToDb(string path)
        {
            var dirInfo = new DirectoryInfo(path);
            var files = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                //ToDo: check if user!=system
                var watch = Stopwatch.StartNew();

                var fileAccessControl = file.GetAccessControl();
                var owner = fileAccessControl.GetOwner(typeof(NTAccount));

                var fileMd5 = CreateMd5ForFile(file.FullName);

                if (_databaseManager.CheckIfHashAndNameExist(fileMd5, file.Name) != null)
                {

                }
                else
                {
                    InsertToDb(file.Name, file.DirectoryName, file.Length.ToString(), fileMd5);
                    _filesManager.SetFileReadOnly(file.FullName);
                }
            }
            inzdbDataSet.GetChanges();
            hashesTableAdapter.Fill(inzdbDataSet.hashes);
            dataGridView1.Refresh();
        }

        private List<FileInfo> GetFilesFromDirectoryRecursively(List<FileInfo> filesList, DirectoryInfo directory)
        {
            try
            {
                foreach (var directoryInfo in directory.GetDirectories())
                {
                    filesList.AddRange(directoryInfo.GetFiles());
                    GetFilesFromDirectoryRecursively(filesList, directoryInfo);
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return filesList;
        }

        private void InsertToDb(string fileName, string path, string fileSize, string fileMd5)
        {
            var dbTableAdapter = new hashesTableAdapter();
            dbTableAdapter.InsertQuery(fileName, path, fileSize, fileMd5);
        }

        public string CreateMd5ForFile(string path)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(path))
                    {
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var response = MessageBox.Show(@"Czy jesteś pewien, że chcesz usunąć ten wpis?", @"Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if ((response == DialogResult.No))
            {
                e.Cancel = true;
            }
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                //delete record and then remove from grid. 
                // you can use your own query but not necessary to use rowid
                var selectedIndex = row.Index;

                // gets the RowID from the first column in the grid
                var rowID = int.Parse(dataGridView1[0, selectedIndex].Value.ToString());
                // string sql = "DELETE FROM Table1 WHERE RowID = @RowID";
                var dbTableAdapter = new hashesTableAdapter();
                dbTableAdapter.DeleteQueryById((rowID));
                // your code for deleting it from the database
                // then your code for refreshing the DataGridView
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            _runningThreads += 1;
            Console.WriteLine(@"Startuje nowy wątek!");
            Console.WriteLine(@"Number of running threads:  " + _runningThreads);
            var r = e.Argument.ToString();
            var args = r.Split('/');
            if (_databaseManager.CheckIfIFleIsRestricted(args[0], args[1]))
            {
                switch (Settings.Default.SecuritySetting)
                {
                    case SecurityMethods.ChangePrivileges:
                        _filesManager.ChangeFilePrivileges(args[0]);
                        Log.CopyingStopped(args[0],_currentUser);
                        break;

                    case SecurityMethods.Delete:
                        _filesManager.DeleteFile(args[0]);
                        Log.CopyingStopped(args[0], _currentUser);
                        break;

                    case SecurityMethods.Substitution:
                        _filesManager.SubstituteFile(args[0]);
                        Log.CopyingStopped(args[0], _currentUser);
                        break;
                }
            }
            else
            {
                //_databaseManager.MarkFileAsSafe(args[0]);
            }
        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _runningThreads -= 1;
            Console.WriteLine(@"Zamykam wątek");
        }

        private void tsbClearDB_Click(object sender, EventArgs e)
        {
            var response = MessageBox.Show(@"Czy jesteś pewien, że chcesz usunąć wszystkie wpisy?", @"Wyczyścić wszystkie wpisy?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if ((response == DialogResult.No)) return;
            var dbTableAdapter = new hashesTableAdapter();
            dbTableAdapter.DeleteAll();
            inzdbDataSet.GetChanges();
            hashesTableAdapter.Fill(inzdbDataSet.hashes);
            dataGridView1.Refresh();
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm(this);
            loginForm.Show(this);
        }

        public void LoginUser(User loginUser)
        {
            user = loginUser;
            MessageBox.Show(@"Ostatnie logowanie miało miejsce: " + user.LastLogin);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Log.Info("Test");
            Log.Error("Test");
            Log.Debug("test");
            Log.Warning("Test");
        }


        private void tsmiActionSubstitution_Click(object sender, EventArgs e)
        {
            Settings.Default.SecuritySetting = SecurityMethods.Substitution;
            Settings.Default.Save();
            SetToolStripButtonBold(SecurityMethods.Substitution);
        }

        private void tsmiActionDelete_Click(object sender, EventArgs e)
        {
            Settings.Default.SecuritySetting = SecurityMethods.Delete;
            Settings.Default.Save();
            SetToolStripButtonBold(SecurityMethods.Delete);
        }

        private void tsmiActionChangePrivileges_Click(object sender, EventArgs e)
        {
            Settings.Default.SecuritySetting = SecurityMethods.ChangePrivileges;
            Settings.Default.Save();
            SetToolStripButtonBold(SecurityMethods.ChangePrivileges);
        }

        private void SetToolStripButtonBold(SecurityMethods securityMethods)
        {
            switch (securityMethods)
            {
                case SecurityMethods.ChangePrivileges:
                    tsmiActionChangePrivileges.Font = new Font(tsmiActionChangePrivileges.Font, FontStyle.Bold);
                    tsmiActionDelete.Font = new Font(tsmiActionDelete.Font, FontStyle.Regular);
                    tsmiActionSubstitution.Font = new Font(tsmiActionSubstitution.Font, FontStyle.Regular);
                    break;

                case SecurityMethods.Delete:
                    tsmiActionChangePrivileges.Font = new Font(tsmiActionChangePrivileges.Font, FontStyle.Regular);
                    tsmiActionDelete.Font = new Font(tsmiActionDelete.Font, FontStyle.Bold);
                    tsmiActionSubstitution.Font = new Font(tsmiActionSubstitution.Font, FontStyle.Regular);
                    break;

                case SecurityMethods.Substitution:
                    tsmiActionChangePrivileges.Font = new Font(tsmiActionChangePrivileges.Font, FontStyle.Regular);
                    tsmiActionDelete.Font = new Font(tsmiActionDelete.Font, FontStyle.Regular);
                    tsmiActionSubstitution.Font = new Font(tsmiActionSubstitution.Font, FontStyle.Bold);
                    break;
            }
        }

        private void tsbLogout_Click(object sender, EventArgs e)
        {
            Settings.Default.LoggedIn = false;
            panel1.Visible = true;
            Manager.StartChecking();
            Watch();
        }

        public void SetUserLoggedIn()
        {
            Settings.Default.LoggedIn = true;
            Manager.Dispose();
            _watcher.Dispose();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            proc.WorkingDirectory = @"F:\_BitBucket\inz\inz\inz\bin\Release\Includes";
            proc.FileName = @"F:\_BitBucket\inz\inz\inz\bin\Release\Includes\EditRegistry.exe";
            //proc.WorkingDirectory = Environment.CurrentDirectory;
            //proc.FileName = Application.ExecutablePath;
            proc.Verb = "runas";
            try
            {
                Process P = Process.Start(proc);
                proc.Arguments=@"F:\_BitBucket\inz\inz\inz\bin\Release\inz.exe";
                P.WaitForExit();
                int result = P.ExitCode;
                var fdsfs = result;
            }
            catch
            {
                // The user refused the elevation.
                // Do nothing and return directly ...
                return;
            }
        }

    }
}
