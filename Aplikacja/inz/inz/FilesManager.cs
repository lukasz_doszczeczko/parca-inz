﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace inz
{
    public class FilesManager
    {
        private ClipboardManager.LogRestrictedFileCopyAttempt _logCopyAttempt;
        public delegate void LogRestrictedFileCopyAttempt(string message);

        private readonly Form _mainForm;

        public delegate void LogWarning(string path);

        private LogWarning _logWarning = Log.WarningCopy;

        private string _currentUser;

        public FilesManager() { }
        
        public FilesManager(Form form, string user)
        {
            _mainForm = form;
            _currentUser = user;
        }


        public string CreateMd5ForFile(string path)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(path))
                    {
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void SetExtendedProporties(string message)
        {
            throw new NotImplementedException();
        }

        public void DeleteFile(string path)
        {
            if (File.Exists(path))
            {

                try
                {
                    File.Delete(path);

                }
                catch (FileNotFoundException ex)
                {
                    _mainForm.Invoke(_logWarning, path);
                }
            }
            else
            {
                _mainForm.Invoke(_logWarning, path);
            }
        }

        public void ChangeFilePrivileges(string path)
        {
            FileInfo file = new FileInfo(path);
            FileSecurity fSecurity = File.GetAccessControl(path);

            // Add the FileSystemAccessRule to the security settings.
            fSecurity.AddAccessRule(new FileSystemAccessRule(_currentUser,
                FileSystemRights.ReadData, AccessControlType.Deny));

            // Set the new access settings.
            file.SetAccessControl(fSecurity);
        }

        public void SetFileReadOnly(string path)
        {
            File.SetAttributes(path, FileAttributes.ReadOnly);
        }

        public void SubstituteFile(string path)
        {
            DeleteFile(path);
            var itemSplited = path.Split(new char[] { '\\' });
            var file = itemSplited[itemSplited.Length - 1];


            var splitedFile = file.Split('.');
            var name = splitedFile[0];
            var extention = splitedFile[1];

            string newPath = "";

            for (int i = 0; i < itemSplited.Length-1; i++)
            {
                newPath += itemSplited[i] + "\\";
            }

            switch (extention)
            {
                case "mp3":
                    FileInfo mp3File = new FileInfo(@"SampleFiles/Sample.mp3");
                    //File.Create();
                    File.Copy(mp3File.FullName,newPath+file);
                    break;

                case "txt":
                    FileInfo txtFile = new FileInfo(@"SampleFiles/Sample.txt");

                    break;

                case "doc":
                    FileInfo docFile = new FileInfo(@"SampleFiles/Sample.doc");

                    break;

                case "pdf":
                    FileInfo pdfFile = new FileInfo(@"SampleFiles/Sample.pdf");

                    break;
            }
        }
    }
}