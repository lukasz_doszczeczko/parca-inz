﻿using System.ComponentModel;

namespace inz.Models
{
    public enum MethodStatus
    {
        [Description("OK")]
        OK,
        [Description("Warning")]
        Warning,
        [Description("Error")]
        Error
    }

    public class StringArray
    {
        public string[] StrArray;
        public MethodStatus Status = MethodStatus.OK;
        public string ErrorMessage = string.Empty;
    }


}