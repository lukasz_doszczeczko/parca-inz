﻿using System;
using System.Windows.Forms;
using inz.inzdbDataSetTableAdapters;

namespace inz
{
    public class DatabaseManager
    {
        public FilesManager FilesManager = new FilesManager();
        const bool FromClipboard = true;
        readonly checkedfilesTableAdapter _checkedFilesTableAdapter = new checkedfilesTableAdapter();

        public object CheckIfHashAndNameExist(string hash, string name)
        {
            var hashesDbTableAdapter = new hashesTableAdapter();
            var inzDbDataTable = new inzdbDataSet.hashesDataTable();
            hashesDbTableAdapter.FillByNameAndMd5(inzDbDataTable, name, hash);
            if (inzDbDataTable.Count != 0)
            {
                return hashesDbTableAdapter;
            }
            else
            {
                return null;
            }

        }

        public object CheckIfNameAndDirExist(string name, string dir)
        {
            var hashesDbTableAdapter = new hashesTableAdapter();
            var inzDbDataTable = new inzdbDataSet.hashesDataTable();
            hashesDbTableAdapter.FillByNameAndDir(inzDbDataTable, name, dir);
            if (inzDbDataTable.Count != 0)
            {
                return hashesDbTableAdapter;
            }
            else
            {
                return null;
            }

        }


        public bool CheckIfIFleIsRestricted(string path, string name)
        {
            string fileMd5 = FilesManager.CreateMd5ForFile(path);
            //TODO: response
            if (fileMd5 == null) return false;

            // File.SetAttributes(path, FileAttributes.ReadOnly);
            var fileExistInDb = CheckIfHashAndNameExist(fileMd5, name);
            if (fileExistInDb != null)
            {
                return true;
            }
            return false;

        }

        public void InsertCheckedFileFromClipboard(string path)
        {
            _checkedFilesTableAdapter.InsertQuery(path, FromClipboard);
        }


        public void MarkFileAsSafe(string path)
        {
            //_checkedFilesTableAdapter.InsertQuery(path, false);
        }


        public bool CheckIffileFromClipboardExistInDb(string path)
        {
            const bool exist = true;
            const bool notExist = false;
            var result = _checkedFilesTableAdapter.GetDataByAndFromClipboard(path, FromClipboard);
            if ( result.Count>0)
                return exist;
            return notExist;
        }
    }
}