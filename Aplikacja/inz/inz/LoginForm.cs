﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using inz.inzdbDataSetTableAdapters;

namespace inz
{
    public partial class LoginForm : Form
    {
        private MainForm mainForm;
        public LoginForm()
        {
            InitializeComponent();
        }

        public LoginForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
        }

        private void tbLogin_Enter(object sender, EventArgs e)
        {
            tbLogin.Text = "";
            tbLogin.ForeColor = Color.Black;
        }

        private void tbLogin_Leave(object sender, EventArgs e)
        {
            if (tbLogin.Text.Equals(""))
            {
                tbLogin.ForeColor = Color.LightGray;  
                tbLogin.Text = "Login";
            }
            else
            {
                tbLogin.ForeColor = Color.Black;  
            }
            
        }

        private void tbPassword_Enter(object sender, EventArgs e)
        {
            tbPassword.Text = "";
            tbPassword.ForeColor = Color.Black;
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (tbPassword.Text.Equals(""))
            {
                tbPassword.ForeColor = Color.LightGray;  
                tbPassword.Text = "Hasło";
            }
            else
            {
                tbPassword.ForeColor = Color.Black;
            }
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            var usersTa = new usersTableAdapter();
            var users = usersTa.GetDataByLoginAndPassword(tbLogin.Text, tbPassword.Text);
            if (users.Count>0)
            {
                User user = new User()
                {
                    Login = users[0].Login,
                    LastLogin = users[0].LastLogin.ToString(CultureInfo.CurrentCulture)
                };
                mainForm.SetUserLoggedIn();
                usersTa.UpdateLastLogin(DateTime.Now, users[0].Id);
                Close();
                mainForm.panel1.Visible = false;
                mainForm.LoginUser(user);
                
            }
            else
            {
                Properties.Settings.Default.LoggedIn = false;
                MessageBox.Show(@"Błędny login i/lub hasło",@"Błąd!",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

    }
}
