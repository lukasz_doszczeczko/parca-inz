﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using inz.Properties;

namespace inz
{
    public class ClipboardManager : IDisposable
    {
        private readonly Form _mainForm;
        public delegate void OnNewFilesFoundDelegate(object sender, string[] collection);
        public event OnNewFilesFoundDelegate OnNewFilesFound;
        public delegate string[] CheckFiles();
        public delegate string[] ClearClipboard(string[] files, string file);
        public delegate string[] ChangeClipboardFiles(string[] files, string file);

        public delegate void SchowMessageForRestrictedFile();
        public delegate void SetClipboardFiles(StringCollection collection);
        public delegate void ClearAllClipboard();
        private string[] _lastFilesFound;
        private static bool _end;
        private readonly FilesManager _filesManager = new FilesManager();
        private readonly DatabaseManager _databaseManager = new DatabaseManager();
        private Thread _checkThread;
        private LogRestrictedFileCoppied _logRestrictedFileCoppied;
        private LogRestrictedFileCopyAttempt _logCopyAttempt;
        private string _currentUser;
        public WaitProgressBarForm progressBarForm;

        public delegate void SchowWaitPB();

        private SchowWaitPB _showWaitPb;
        public delegate void LogRestrictedFileCoppied(string message, string user);

        public delegate void LogRestrictedFileCopyAttempt(string message, string user);

        public void StartChecking()
        {
            _end = false;
            _checkThread = new Thread(CheckClipboard);
            _checkThread.Start();
        }


        public ClipboardManager(Form form, string user)
        {
            _currentUser = user;
            _mainForm = form;
            _mainForm.FormClosing += _mainForm_FormClosing;
            _logRestrictedFileCoppied = Log.RestrictedFileCopy;
            _logCopyAttempt = Log.CopyingStopped;

        }

        void _mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _end = true;
        }

        public string[] Clear(string[] files, string file)
        {
            List<string> filesList = files.ToList();
            var indexOfFile = FindIndexOf(file, filesList);
            filesList.RemoveAt(indexOfFile);
            Clipboard.Clear();
            StringCollection collection = new StringCollection();
            collection.AddRange(filesList.ToArray());
            if (collection.Count > 0)
            {
                Clipboard.SetFileDropList(collection);
            }
            return filesList.ToArray();
        }

        private int FindIndexOf(string file, List<string> filesList)
        {
            int i = 0;
            foreach (var fileListItem in filesList)
            {
                if (fileListItem.Equals(file))
                {
                    return i;
                }
                i++;
            }
            return -1;
        }

        public string[] ChangeFile(string[] files, string file)
        {
            var splitedFile = file.Split('.');
            var name = splitedFile[0];
            var extention = splitedFile[1];


            //Clipboard.Clear();

            files = Clear(files, file);


            StringCollection filesCollection = new StringCollection();
            filesCollection.AddRange(files.ToArray());
            var filesList = files.ToList();

            switch (extention)
            {
                case "mp3":
                    FileInfo oldFile = new FileInfo(file);
                    File.Copy(@"SampleFiles/Sample.mp3", @"SampleFiles/Tmp/" + oldFile.Name);
                    FileInfo mp3File = new FileInfo(@"SampleFiles/Tmp/" + oldFile.Name);
                    filesCollection.Add(mp3File.FullName);
                    filesList.Add(mp3File.FullName);
                    break;

                case "txt":
                    FileInfo oldTxtFile = new FileInfo(file);
                    File.Copy(@"SampleFiles/Sample.txt", @"SampleFiles/Tmp/" + oldTxtFile.Name);
                    FileInfo txtFile = new FileInfo(@"SampleFiles/Tmp/" + oldTxtFile.Name);
                    filesCollection.Add(txtFile.FullName);
                    filesList.Add(txtFile.FullName);
                    //Clipboard.SetFileDropList(filesCollection);
                    break;

                case "doc":
                    FileInfo oldDocFile = new FileInfo(file);
                    File.Copy(@"SampleFiles/Sample.doc", @"SampleFiles/Tmp/" + oldDocFile.Name);
                    FileInfo docFile = new FileInfo(@"SampleFiles/Tmp/" + oldDocFile.Name);
                    filesCollection.Add(docFile.FullName);
                    filesList.Add(docFile.FullName);
                    //Clipboard.SetFileDropList(filesCollection);
                    break;

                case "pdf":
                    FileInfo oldPdfFile = new FileInfo(file);
                    File.Copy(@"SampleFiles/Sample.pdf", @"SampleFiles/Tmp/" + oldPdfFile.Name);
                    FileInfo pdfFile = new FileInfo(@"SampleFiles/Tmp/" + oldPdfFile.Name);
                    filesCollection.Add(pdfFile.FullName);
                    filesList.Add(pdfFile.FullName);
                    //Clipboard.SetFileDropList(filesCollection);
                    break;

                default:
                    FileInfo oldFileWithoutExtention = new FileInfo(file);
                    File.Copy(@"SampleFiles/Sample.txt", @"SampleFiles/Tmp/" + oldFileWithoutExtention.Name + ".txt");
                    FileInfo fileWithoutExtention = new FileInfo(@"SampleFiles/Tmp/" + oldFileWithoutExtention.Name + ".txt");
                    filesCollection.Add(fileWithoutExtention.FullName);
                    filesList.Add(fileWithoutExtention.FullName);
                    break;
            }
            return filesList.ToArray();
        }

        private void ClearTmpFolder()
        {

            DirectoryInfo directory = new DirectoryInfo(@"SampleFiles/Tmp/");

            foreach (FileInfo file in directory.GetFiles()) file.Delete();
            foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);

        }

        public void ShowPb()
        {
            progressBarForm = new WaitProgressBarForm();
            progressBarForm.ShowDialog();
        }

        public void ShowMessage()
        {
            MessageBox.Show(@"Nastąpiła próba nieuprawnionego kopiowania plików!", @"Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void CheckClipboard()
        {
            while (!_end)
            {
                try
                {
                    string[] tmp = (string[])_mainForm.Invoke(new CheckFiles(GetFilesOrDirectory));
                    if (tmp.Any())
                    {
                        
                        if (_lastFilesFound != null && tmp.Length == _lastFilesFound.Length)
                        {

                            for (int i = 0; i < tmp.Length; i++)
                            {
                                if (tmp[i] != _lastFilesFound[i])
                                {
                                    if (OnNewFilesFound != null)
                                    {
                                        //var showPb = new Thread(ShowPb);
                                        //showPb.Start();
                                        ClearTmpFolder();
                                        ClearAllClipboard clearAll = Clipboard.Clear;
                                        _mainForm.Invoke(clearAll);
                                        foreach (var item in tmp)
                                        {
                                            if (!_databaseManager.CheckIffileFromClipboardExistInDb(item))
                                            {
                                                _filesManager.CreateMd5ForFile(item);
                                                string fileMd5 = _filesManager.CreateMd5ForFile(item);
                                                var itemSplited = item.Split('\\');

                                                var name = itemSplited[itemSplited.Length - 1];
                                                var fileIsRestricted = _databaseManager.CheckIfHashAndNameExist(
                                                    fileMd5, name);
                                                if (fileIsRestricted != null)
                                                {
                                                    object newClipboardFiles;
                                                    switch (Settings.Default.SecuritySetting)
                                                    {
                                                        case SecurityMethods.ChangePrivileges:

                                                            break;

                                                        case SecurityMethods.Delete:
                                                            ClearClipboard clearClipboard = Clear;
                                                            newClipboardFiles = _mainForm.Invoke(clearClipboard, tmp,
                                                                item);
                                                            tmp = (string[])newClipboardFiles;
                                                            break;

                                                        case SecurityMethods.Substitution:
                                                            ChangeClipboardFiles changeClipboardFiles = ChangeFile;
                                                            newClipboardFiles = _mainForm.Invoke(changeClipboardFiles,
                                                                tmp, item);
                                                            tmp = (string[])newClipboardFiles;
                                                            break;
                                                    }
                                                    _mainForm.Invoke(_logRestrictedFileCoppied, item, _currentUser);
                                                    OnNewFilesFound(this, tmp);
                                                }
                                                else
                                                {
                                                    if (!_databaseManager.CheckIffileFromClipboardExistInDb(item))
                                                    {
                                                        _databaseManager.InsertCheckedFileFromClipboard(item);
                                                    }
                                                }


                                            }
                                        }
                                        SetClipboardFiles setClipboardFiles = Clipboard.SetFileDropList;
                                        _lastFilesFound = tmp;
                                        StringCollection collection = new StringCollection();
                                        collection.AddRange(tmp);
                                        _mainForm.Invoke(setClipboardFiles, collection);

                                        //showPb.Abort();

                                    }
                                    break;
                                }
                            }
                        }
                        else
                        {

                            if (OnNewFilesFound != null)
                            {

                                //var showPb = new Thread(ShowPb);
                                //showPb.Start();
                                ClearTmpFolder();
                                ClearAllClipboard clearAll = Clipboard.Clear;
                                _mainForm.Invoke(clearAll);
                                foreach (var item in tmp)
                                {
                                    if (!_databaseManager.CheckIffileFromClipboardExistInDb(item))
                                    {
                                        _filesManager.CreateMd5ForFile(item);
                                        string fileMd5 = _filesManager.CreateMd5ForFile(item);
                                        var itemSplited = item.Split('\\');


                                        var name = itemSplited[itemSplited.Length - 1];
                                        var fileIsRestricted = _databaseManager.CheckIfHashAndNameExist(fileMd5, name);
                                        if (fileIsRestricted != null)
                                        {
                                            object newClipboardFiles;
                                            switch (Settings.Default.SecuritySetting)
                                            {
                                                case SecurityMethods.ChangePrivileges:

                                                    break;

                                                case SecurityMethods.Delete:
                                                    ClearClipboard clearClipboard = Clear;
                                                    newClipboardFiles = _mainForm.Invoke(clearClipboard, tmp, item);
                                                    tmp = (string[])newClipboardFiles;
                                                    break;

                                                case SecurityMethods.Substitution:
                                                    ChangeClipboardFiles changeClipboardFiles = ChangeFile;
                                                    newClipboardFiles = _mainForm.Invoke(changeClipboardFiles, tmp, item);
                                                    tmp = (string[])newClipboardFiles;
                                                    break;
                                            }
                                            _mainForm.Invoke(_logRestrictedFileCoppied, item, _currentUser);
                                            OnNewFilesFound(this, tmp);
                                        }


                                    }
                                }
                                SetClipboardFiles setClipboardFiles = Clipboard.SetFileDropList;
                                _lastFilesFound = tmp;
                                StringCollection collection = new StringCollection();
                                collection.AddRange(tmp);
                                _mainForm.Invoke(setClipboardFiles, collection);
                                //showPb.Abort();
                            }

                        }
                        
                    }
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    Log.Error("Clipboard CheckClipboard:    " + ex.Message);
                    Thread.Sleep(100);
                }
            }
        }

        public void SecureRestrictedFile(string name)
        {

        }

        public string GetText()
        {
            if (Clipboard.ContainsText(TextDataFormat.UnicodeText))
            {
                return Clipboard.GetText(TextDataFormat.UnicodeText);
            }
            if (Clipboard.ContainsText(TextDataFormat.Text))
            {
                return Clipboard.GetText(TextDataFormat.Text);
            }
            if (Clipboard.ContainsText(TextDataFormat.Text))
            {
                return Clipboard.GetText(TextDataFormat.Text);
            }
            if (Clipboard.ContainsText(TextDataFormat.Html))
            {
                return Clipboard.GetText(TextDataFormat.Html);
            }

            return null;
        }

        public string[] GetFilesOrDirectory()
        {
            StringCollection filesCollection;
            List<string> filesList = new List<string>();

            try
            {
                filesCollection = Clipboard.GetFileDropList();
                foreach (var listItem in filesCollection)
                {
                    // get the file attributes for file or directory
                    FileAttributes attr = File.GetAttributes(listItem);

                    //detect whether its a directory or file
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(listItem);
                        var files = directoryInfo.GetFiles("*.*", SearchOption.AllDirectories);
                        foreach (var file in files)
                        {
                            filesList.Add(file.FullName);

                        }
                    }
                    else
                    {
                        filesList.Add(listItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Clipboard GetFilesOrDirectory:   " + ex.Message);
            }
            return filesList.ToArray();
        }

        protected virtual void OnOnNewFilesFound(string[] collection)
        {
            var handler = OnNewFilesFound;
            if (handler != null) handler(this, collection);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _end = true;
                _checkThread.Join(1000);
            }
        }

        ~ClipboardManager()
        {
            Dispose();
        }
    }
}