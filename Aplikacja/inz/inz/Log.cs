﻿using System;
using System.IO;
using System.Windows.Forms;

namespace inz
{
    public static class Log
    {
        private static string RestrictedFileCoppiedLogPath= CheckIfFileExistAndReturn(AppDomain.CurrentDomain.BaseDirectory+@"logs\RestrictedFileCoppied.txt");
        private static string WarningsLogPath=CheckIfFileExistAndReturn(AppDomain.CurrentDomain.BaseDirectory+@"logs\Warnings.txt");
        private static string DevelopmentLogPath = CheckIfFileExistAndReturn(AppDomain.CurrentDomain.BaseDirectory+@"logs\DevelopmentLog.txt");
        private static string _currentUser;
        //static Log()
        //{
        //    //RestrictedFileCoppiedLogPath = CheckIfFileExistAndReturn(@"logs\RestrictedFileCoppied.txt");
        //    //WarningsLogPath = CheckIfFileExistAndReturn(@"logs\Warnings.txt");
        //    //DevelopmentLogPath = CheckIfFileExistAndReturn(@"logs\DevelopmentLog.txt");
        //}

        private static string CheckIfFileExistAndReturn(string path)
        {
            var log1File = new FileInfo(path);
            if (!log1File.Exists)
            {
                File.Create(path);
                CheckIfFileExistAndReturn(path);
                return "";
            }
            return log1File.FullName;
        }

        public static void RestrictedFileCopy(string file, string currentUser)
        {
            File.AppendAllText(RestrictedFileCoppiedLogPath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     Restricted file " + file + @" was copied by "+currentUser+Environment.NewLine);
        }

        public static void CopyingStopped(string file, string currentUser)
        {
            File.AppendAllText(RestrictedFileCoppiedLogPath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     Restricted file ("+file+@") copying - attempt terminated. User : "+currentUser+ Environment.NewLine);
        }

        public static void WarningCopy(string file)
        {
            File.AppendAllText(WarningsLogPath,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     Copying file " + file +
                @" was detected but something went wrong" + Environment.NewLine);
        }

        public static void Warning(string message)
        {
            File.AppendAllText(DevelopmentLogPath,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     WARNING   " + message +
                @"  " + Environment.NewLine);
        }

        public static void Error(string message)
        {
            File.AppendAllText(DevelopmentLogPath,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     ERROR   " + message +
                @"  " + Environment.NewLine);
        }

        public static void Info(string message)
        {
            File.AppendAllText(DevelopmentLogPath,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     INFO   " + message +
                @"  " + Environment.NewLine);
        }

        public static void Debug(string message)
        {
            File.AppendAllText(DevelopmentLogPath,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"     DEBUG   " + message +
                @"  " + Environment.NewLine);
        }
    }
}