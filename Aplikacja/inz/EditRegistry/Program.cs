﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace EditRegistry
{
    class Program
    {
        static int Main(string[] args)
        {
            //var admin = IsAdministrator();

            //if (admin)
            //{
                
            //}
            //ProcessStartInfo proc = new ProcessStartInfo();
            //proc.UseShellExecute = true;
            //proc.WorkingDirectory = Environment.CurrentDirectory;
            //proc.FileName = Application.ExecutablePath;
            //proc.Verb = "runas";
            //try
            //{
            //    Process.Start(proc);
            //}
            //catch
            //{
            //    // The user refused the elevation.
            //    // Do nothing and return directly ...
            //    return;
            //}
            //Application.Exit();  // Quit itself
            try
            {

                Microsoft.Win32.RegistryKey key;
                key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
                key.SetValue("mysqld", @"C:\xampp\mysql\bin\mysqld.exe");
                key.Close();


                key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
                key.SetValue("inz", args[0]);
                key.Close();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return 1;
            }
        }
        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
